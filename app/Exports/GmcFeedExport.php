<?php

namespace App\Exports;

use App\Helpers\CrawlProductHelper;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class GmcFeedExport implements FromArray, WithMapping, WithHeadings
{
    use Exportable;

    const CONDITION = 'New';
    const AVAILABILITY = 'in stock';
    const IDENTIFIER_EXISTS = 'no';
    const RESTRICT_WORDS = [
        'Nike',
        'Adidas',
        'Authentic',
        'Replica',
        'â€“',
        '&#8211;',
        '‰ÛÒ',
        '&#8217;',
    ];

    const IN_STOCK_PLAYERS = [
        "LeBron James",
        "Stephen Curry",
        "Giannis Antetokounmpo",
        "Kawhi Leonard",
        "Russell Westbrook",
        "Kyrie Irving",
        "Zion Williamson",
        "Kevin Durant",
        "Vince Carter",
        "Allen Iverson",
        "Anthony Davis",
        "Ben Simmons",
        "James Harden",
        "Kemba Walker",
        "Joel Embiid",
        "Luka Doncic",
        "Jayson Tatum",
        "Damian Lillard",
        "Kobe Bryant",
        "Larry Bird",
        "Michael Jordan",
        "Donovan Mitchell",
        "Paul George",
        "Tracy McGrady",
        "Dwyane Wade",
        "Magic Johnson",
        "RJ Barrett",
        "De'Aaron Fox",
        "Klay Thompson",
        "Nikola Jokic",
        "Jimmy Butler",
        "Scottie Pippen",
        "Ja Morant",
        "Trae Young",
        "Kyle Kuzma",
        "Patrick Ewing",
        "Dirk Nowitzki",
        "Pascal Siakam",
        "Penny Hardaway",
        "Tim Duncan",
        "Lou Williams",
        "Kristaps Porzingis",
        "Devin Booker",
        "Kareem Abdul-Jabbar",
        "Lonzo Ball",
        "Hakeem Olajuwon",
        "Kyle Lowry",
        "Demar Derozan",
        "Shaquille O'Neal",
        "Blake Griffin",
        "Patrick Mahomes",
        "Tom Brady",
        "Khalil Mack",
        "Baker Mayfield",
        "Ezekiel Elliott",
        "Saquon Barkley",
        "Dak Prescott",
        "JuJu Smith-Schuster",
        "Drew Brees",
        "Aaron Rodgers",
        "Jimmy Garoppolo",
        "Carson Wentz",
        "Alvin Kamara",
        "Julian Edelman",
        "Odell Beckham Jr",
        "Adam Thielen",
        "Leighton Vander Esch",
        "Lamar Jackson",
        "Daniel Jones",
        "Russell Wilson",
        "Josh Allen (Bills)",
        "Gardner Minshew II",
        "Christian McCaffrey",
        "Walter Payton",
        "George Kittle",
        "Sam Darnold",
        "Zach Ertz",
        "Jason Witten",
        "Aaron Donald",
        "Derek Carr",
        "Julio Jones",
        "TJ Watt",
        "Jamal Adams",
        "Mitchell Trubisky",
        "Jaylon Smith",
        "Amari Cooper",
        "Josh Jacobs",
        "Deshaun Watson",
        "James Conner",
        "Von Miller",
        "Brian Dawkins",
        "Todd Gurley",
        "Jerry Rice",
        "Deion Sanders",
        "Travis Kelce",
        "Michael Thomas",
        "JJ Watt",
        "Rob Gronkowski",
        "Antonio Brown",
        "Phillip Lindsay",
        "Jose Altuve",
        "Aaron Judge",
        "Cody Bellinger",
        "Javier Baez",
        "Ronald Acuna Jr",
        "Alex Bregman",
        "Pete Alonso",
        "Juan Soto",
        "Christian Yelich",
        "Bryce Harper",
        "Max Scherzer",
        "Yadier Molina",
        "Mike Trout",
        "Gleyber Torres",
        "Anthony Rizzo",
        "Mookie Betts",
        "Clayton Kershaw",
        "Freddie Freeman",
        "Mariano Rivera",
        "George Springer",
        "Trea Turner",
        "Kris Bryant",
        "Justin Verlander",
        "Paul Goldschmidt",
        "Ken Griffey Jr",
        "Nolan Ryan",
        "Mickey Mantle",
        "Carlos Correa",
        "Stephen Strasburg",
        "Francisco Lindor",
        "Gerrit Cole",
        "Anthony Rendon",
        "David Ortiz",
        "Justin Turner",
        "Gary Sanchez",
        "Jacob deGrom",
        "Mike Piazza",
        "Andrew Benintendi",
        "Kike Hernandez",
        "Vladimir Guerrero Jr",
        "Didi Gregorius",
        "Buster Posey",
        "Shohei Ohtani",
        "Giancarlo Stanton",
        "Matt Chapman",
        "DJ LeMahieu",
        "Dansby Swanson",
        "Nolan Arenado",
        "Corey Seager",
        "Mike Schmidt",
        "Alex Ovechkin",
        "Marc-Andre Fleury",
        "Vladimir Tarasenko",
        "Jordan Binnington",
        "Sidney Crosby",
        "Ryan O'Reilly",
        "Nathan MacKinnon",
        "TJ Oshie",
        "Brent Burns",
        "Tom Wilson",
        "PK Subban",
        "Auston Matthews",
        "Patrice Bergeron",
        "Jack Hughes",
        "Artemi Panarin",
        "Carter Hart",
        "David Pastrnak",
        "Patrick Kane",
        "Connor McDavid",
        "Brad Marchand",
        "Alex Pietrangelo",
        "Nikita Kucherov",
        "Claude Giroux",
        "Jonathan Toews",
        "Wayne Gretzky",
        "Mark Stone",
        "Henrik Lundqvist",
        "Zdeno Chara",
        "Braden Holtby",
        "Mathew Barzal",
        "Erik Karlsson",
        "William Karlsson",
        "Jack Eichel",
        "Cale Makar",
        "Dylan Larkin",
        "Kaapo Kakko",
        "Mika Zibanejad",
        "Pekka Rinne",
        "Nico Hischier",
        "Steven Stamkos",
        "Andrei Svechnikov",
        "Ryan Getzlaf",
        "Max Pacioretty",
        "Colton Parayko",
        "Joe Pavelski",
        "John Tavares",
        "James Neal",
        "Evgeni Malkin",
        "Roman Josi",
        "Anze Kopitar",
    ];

    private $input;

    public function __construct($data)
    {
        $this->input = $data;
    }

    public function headings(): array
    {
        return [
            'id',
            'title',
            'description',
            'condition',
            'price',
            'availability',
            'link',
            'image link',
            'identifier exists',
            'custom_label_0',
            'custom_label_1',
            'custom_label_2',
            'custom_label_3',
            'custom_label_4',
        ];
    }

    public function array(): array
    {
        $feedContent = [];
        $productPrices = [];

        foreach ($this->input['excelContent'] as $key => $item) {

            $productId = strval($item['ID']);
            $parentId = strval($item['post_parent']);

            if ($this->input['idPrefix'] !== '') {
                $productId = $this->input['idPrefix'] . '-' . $productId;
            }
            
            // $genProductId = $this->setProductId($productId, $this->input['replaceDomain']);
            $category = isset($item['tax:product_cat']) ? trim($item['tax:product_cat']) : '';

            // Parent products.
            if ($parentId === '') {

                // Get image.
                $images = explode('|', trim($item['images']));
                $mainImage = trim($images[0]);
                if (strpos($mainImage, ' ') !== false) {
                    $temp = explode(' ', $mainImage);
                    $mainImage = trim($temp[0]);
                }

                // Apple image URLs.
                if ($this->input['applyImageUrl'] === '1') {
                    $mainImage = $this->replaceDomain($mainImage, $this->input['replaceDomain']);
                }

                // Get product link.
                $productUrl = $this->replaceDomain($item['product_page_url'], $this->input['replaceDomain']);

                // If this is simple products.
                $price = '';
                if ($this->input['productType'] === 'simple') {
                    $price = $this->setPrice($item);
                }

                $productTitle = $this->input['titlePrefix'] . $this->cleanTitle($item['post_title']) . ' ' . $this->input['titleSuffix'];

                // Custom label 0 - In_stock_player.
                $customLabel0 = '';
                $lowerTitle = mb_strtolower($productTitle);
                foreach (self::IN_STOCK_PLAYERS as $inStockPlayer) {
                    $inStockPlayer = mb_strtolower($inStockPlayer);
                    if (strpos($lowerTitle, $inStockPlayer) !== false) {
                        $customLabel0 = 'in_stock_player';
                    }
                }

                $feedContent[$productId] = [
                    'id' => $productId,
                    'title' => $productTitle,
                    'description' => $this->cleanRestrictWords($this->input['descriptionPrefix'] . $item['post_content'] . $this->input['descriptionSuffix']),
                    'condition' => self::CONDITION,
                    'price' => $price,
                    'availability' => self::AVAILABILITY,
                    'link' => $productUrl,
                    'image link' => $mainImage,
                    'identifier exists' => self::IDENTIFIER_EXISTS,
                    'custom_label_0' => $customLabel0,
                    'custom_label_1' => $category,
                    'custom_label_2' => '',
                    'custom_label_3' => '',
                    'custom_label_4' => '',
                ];

            }
            // Variants.
            else {
                if ($this->input['idPrefix'] !== '') {
                    $parentId = $this->input['idPrefix'] . '-' . $parentId;
                }
                if (!isset($productPrices[$parentId])) {
                    $productPrices[$parentId] = $this->setPrice($item);
                }
            }
        }

        // Add price to feed for variation products.
        if ($this->input['productType'] === 'variation') {
            foreach ($feedContent as $productId => $feedItem) {
                $feedContent[$productId]['price'] = $productPrices[$productId];
            }
        }

        // Shuffle feed content.
        shuffle($feedContent);
        
        return $feedContent;
    }

    private function setProductId($id, $domain)
    {
        return $id;

//        $prefix = substr($domain, 0, 3);
//        $prefix = strtoupper($prefix);
//        return $prefix . "-{$id}-" . rand(1000, 9999);
    }

    /**
     * @param $item
     * @return string
     */
    private function setPrice($item)
    {
        $regularPrice = floatval($item['regular_price']);
        $salePrice = floatval($item['sale_price']);

        if ($salePrice > 0) {
            $price = $salePrice;
        } else {
            $price = $regularPrice;
        }

        return 'USD ' . $price;
    }

    public function map($row): array
    {
        return [
            $row['id'],
            $row['title'],
            $row['description'],
            $row['condition'],
            $row['price'],
            $row['availability'],
            $row['link'],
            $row['image link'],
            $row['identifier exists'],
            $row['custom_label_0'],
            $row['custom_label_1'],
            $row['custom_label_2'],
            $row['custom_label_3'],
            $row['custom_label_4'],
        ];
    }

    /**
     * @param $url
     * @param $newDomain
     * @return string|string[]
     */
    private function replaceDomain($url, $newDomain)
    {
        $temp = parse_url($url);
        if ($url === '') {
            return '';
        }

        return str_replace($temp['host'], $newDomain, $url);
    }

    /**
     * @param $title
     * @return string|string[]|null
     */
    private function cleanTitle($title)
    {
        $title = trim($title);
        $title = remove_bs($title);
        foreach (self::RESTRICT_WORDS as $restrictWord) {
            if (strpos($title, $restrictWord) !== false) {
                $title = str_replace($restrictWord, '', $title);
            }
        }
        $title = preg_replace('!\s+!', ' ', $title);

        return $title;
    }

    /**
     * @param $string
     * @return string|string[]
     */
    private function cleanRestrictWords($string)
    {
        foreach (self::RESTRICT_WORDS as $restrictWord) {
            if (strpos($string, $restrictWord) !== false) {
                $string = str_replace($restrictWord, '', $string);
            }
        }

        return $string;
    }
}



