<?php

namespace App\Exports;

use App\Helpers\CrawlProductHelper;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class CrawlProductExportToShopify implements FromArray, WithMapping, WithHeadings
{
    use Exportable;
    public $importData;

    const DEFAULT_IMAGE = 'https://fanzix.com/wp-content/uploads/2019/05/logo_icon-512x512.png';
    const VENDOR = 'Fanyeez';
    const PUBLISHED = 'TRUE';
    const OPTION1 = 'Size';
    const OPTION2 = 'Size system';
    const OPTION2_VALUE = 'Men|Women';
    const VARIANT_GRAMS = '0';
    const VARIANT_INVENTORY_TRACKER = 'shopify';
    const VARIANT_INVENTORY_QTY = '0';
    const VARIANT_INVENTORY_POLICY = 'deny';
    const VARIANT_FULFILLMENT_SERVICE = 'manual';
    const VARIANT_REQUIRES_SHIPPING = 'TRUE';
    const VARIANT_TAXABLE = 'TRUE';
    const GIFT_CARD = 'FALSE';
    const VARIANT_WEIGHT_UNIT = 'kg';

    public function __construct($importData)
    {
        $this->importData = $importData;
    }

    public function headings(): array
    {
        return [
            "Handle",
            "Title",
            "Body (HTML)",
            "Vendor",
            "Tags",
            "Published",
            "Option1 Name",
            "Option1 Value",
            "Option2 Name",
            "Option2 Value",
            "Variant SKU",
            "Variant Grams",
            "Variant Inventory Tracker",
            "Variant Inventory Qty",
            "Variant Inventory Policy",
            "Variant Fulfillment Service",
            "Variant Price",
            "Variant Compare At Price",
            "Variant Requires Shipping",
            "Variant Taxable",
            "Image Src",
            "Image Position",
            "Gift Card",
            "Variant Weight Unit"
        ];
    }

    private function cleanTitle($string)
    {
        $string = preg_replace('/[^A-Za-z0-9\-\s]/', '', $string); // Removes special chars.
        return $string;
    }

    public function array(): array
    {
        $data = [];
        $collection = $this->importData;

        foreach ($collection as $item) {
            $sizeSystemArray = explode('|', self::OPTION2_VALUE);
            $sizeListArray = explode("|", '4|4.5|5|5.5|6|6.5|7|7.5|8|8.5|9|9.5|10|10.5|11|11.5|12');
            $handle = $this->clean($item->name) . '-' . $this->generateRandomString(5);
            $title = $this->cleanTitle(trim($item->name));

            if ($title === '') {
                continue;
            }

            $skuItem = 'fnz' . $this->generateRandomNumberString(6);
            $regularPrice = $item->price;
            $itemSalePrice = CrawlProductHelper::setSalePrice($item->price);
            $finalImages = explode('|', $item->images);
            $imageIndex = 0;
            $imagePosition = 1;

            $data[] = [
                "Handle" => $handle,
                "Title" => $title,
                "Body (HTML)" => trim($item->description),
                "Vendor" => self::VENDOR,
                "Tags" => strtolower($title),
                "Published" => self::PUBLISHED,
                "Option1 Name" => self::OPTION1,
                "Option1 Value" => $sizeListArray[0],
                "Option2 Name" => self::OPTION2,
                "Option2 Value" => $sizeSystemArray[0],
                "Variant SKU" => $skuItem,
                "Variant Grams" => self::VARIANT_GRAMS,
                "Variant Inventory Tracker" => self::VARIANT_INVENTORY_TRACKER,
                "Variant Inventory Qty" => self::VARIANT_INVENTORY_QTY,
                "Variant Inventory Policy" => self::VARIANT_INVENTORY_POLICY,
                "Variant Fulfillment Service" => self::VARIANT_FULFILLMENT_SERVICE,
                "Variant Price" => $itemSalePrice,
                "Variant Compare At Price" => $regularPrice,
                "Variant Requires Shipping" => self::VARIANT_REQUIRES_SHIPPING,
                "Variant Taxable" => self::VARIANT_TAXABLE,
                "Image Src" => isset($finalImages[$imageIndex]) ? trim($finalImages[$imageIndex]) : '',
                "Image Position" => $imagePosition,
                "Gift Card" => self::GIFT_CARD,
                "Variant Weight Unit" => self::VARIANT_WEIGHT_UNIT,
            ];

            $imageIndex++;
            $imagePosition++;

            foreach ($sizeListArray as $k => $sizeItem) {
                foreach ($sizeSystemArray as $j => $sizeSystem) {
                    if ($k === 0 && $j === 0) {
                        continue;
                    }

                    $data[] = [
                        "Handle" => $handle,
                        "Title" => "",
                        "Body (HTML)" => "",
                        "Vendor" => "",
                        "Tags" => "",
                        "Published" => "",
                        "Option1 Name" => "",
                        "Option1 Value" => $sizeItem,
                        "Option2 Name" => "",
                        "Option2 Value" => $sizeSystem,
                        "Variant SKU" => $skuItem . '-' . $sizeSystem . '-' . $k,
                        "Variant Grams" => self::VARIANT_GRAMS,
                        "Variant Inventory Tracker" => self::VARIANT_INVENTORY_TRACKER,
                        "Variant Inventory Qty" => self::VARIANT_INVENTORY_QTY,
                        "Variant Inventory Policy" => self::VARIANT_INVENTORY_POLICY,
                        "Variant Fulfillment Service" => self::VARIANT_FULFILLMENT_SERVICE,
                        "Variant Price" => $itemSalePrice,
                        "Variant Compare At Price" => $regularPrice,
                        "Variant Requires Shipping" => self::VARIANT_REQUIRES_SHIPPING,
                        "Variant Taxable" => self::VARIANT_TAXABLE,
                        "Image Src" => isset($finalImages[$imageIndex]) ? trim($finalImages[$imageIndex]) : '',
                        "Image Position" => isset($finalImages[$imageIndex]) ? $imagePosition : '',
                        "Gift Card" => "",
                        "Variant Weight Unit" => self::VARIANT_WEIGHT_UNIT,
                    ];

                    $imageIndex++;
                    $imagePosition++;
                }
            }

            // update
            $item->update(['is_exported' => 1]);
        }

        return $data;
    }

    public function map($row): array
    {
        return [
            $row["Handle"],
            $row["Title"],
            $row["Body (HTML)"],
            $row["Vendor"],
            $row["Tags"],
            $row["Published"],
            $row["Option1 Name"],
            $row["Option1 Value"],
            $row["Option2 Name"],
            $row["Option2 Value"],
            $row["Variant SKU"],
            $row["Variant Grams"],
            $row["Variant Inventory Tracker"],
            $row["Variant Inventory Qty"],
            $row["Variant Inventory Policy"],
            $row["Variant Fulfillment Service"],
            $row["Variant Price"],
            $row["Variant Compare At Price"],
            $row["Variant Requires Shipping"],
            $row["Variant Taxable"],
            $row["Image Src"],
            $row["Image Position"],
            $row["Gift Card"],
            $row["Variant Weight Unit"],
        ];
    }

    private function clean($string)
    {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        $string = strtolower($string); // Convert to lowercase

        return $string;
    }

    private function generateRandomNumberString($length = 10)
    {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    private function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}



























