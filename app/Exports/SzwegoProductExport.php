<?php

namespace App\Exports;

use App\Helpers\CrawlProductHelper;
use App\Models\CrawlProduct;
use App\Models\SzwegoProduct;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class SzwegoProductExport implements FromArray, WithMapping, WithHeadings
{
    use Exportable;

    public function __construct()
    {

    }

    public function headings(): array
    {
        return [
            'url',
//            'title',
            'price',
        ];
    }

    public function array(): array
    {
        $data = [];
        $collection = SzwegoProduct::where('szwego_shop_id', 1)->get();
        foreach ($collection as $item) {
            $data[] = [
                'url' => $item->url,
//                'title' => $item->title,
                'price' => $item->price,
            ];
        }

        return $data;
    }

    public function map($row): array
    {
        return [
            $row['url'],
//            $row['title'],
            $row['price'],
        ];
    }
}



























