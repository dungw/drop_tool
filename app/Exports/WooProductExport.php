<?php

namespace App\Exports;

use App\Helpers\CrawlProductHelper;
use App\Models\CrawlProduct;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class WooProductExport implements FromArray, WithMapping, WithHeadings
{
    use Exportable;

    public $data = [];

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function headings(): array
    {
        return [
            'name',
            'sku',
            'product_page_url',
        ];
    }

    public function array(): array
    {
        $collection = $this->data;

        foreach ($collection as $k => $item) {

            if (trim($item['images']) === "") {
                unset($collection[$k]);
                continue;
            }

//            $image = explode('!', $item['images']);
//            $collection[$k]['images'] = trim($image[0]);
        }

        return array_values($collection);
    }

    public function map($row): array
    {
        return [
            $row['post_title'],
            $row['sku'],
            $row['product_page_url'],
        ];
    }
}



























