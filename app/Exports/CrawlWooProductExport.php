<?php

namespace App\Exports;

use App\Helpers\CrawlProductHelper;
use App\Models\CrawlProduct;
use App\Models\WooProduct;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class CrawlWooProductExport implements FromArray, WithMapping, WithHeadings
{
    use Exportable;
    private $limitRow;

    const WOO_SITE_ID = 12;
    const FILTER_CATEGORY = 'Jersey';
    const PRODUCT_TYPE = 'simple';
    const CATEGORY_OUTPUT = 'NBA';
    const PRICE_DISCOUNT_PERCENT = 0;
    const DECREASE_AMOUNT_PRICE = 0;
    const DECREASE_PERCENT_PRICE_LOWER_80 = 30;
    const DECREASE_PERCENT_PRICE_LOWER_120 = 40;
    const DECREASE_PERCENT_PRICE_HIGHER_120 = 50;

    private $specialChars = [];

    public function __construct($limitRow)
    {
        $this->limitRow = $limitRow;
    }

    public function headings(): array
    {
        if (self::PRODUCT_TYPE === 'simple') {
            $heading = [
                'sku',
                'post_title',
                'post_excerpt',
                'post_content',
                'post_status',
                'regular_price',
                'sale_price',
                'stock_status',
                'stock',
                'manage_stock',
                'Images',
                'tax:product_type',
                'tax:product_cat',
                'tax:product_tag',
            ];
        } elseif (self::PRODUCT_TYPE === 'variation') {
            $heading = [
                'parent_sku',
                'sku',
                'meta:attribute_size',
                'attribute:Size',
                'attribute_data:Size',
                'attribute_default:Size',
                'post_title',
                'post_excerpt',
                'post_content',
                'post_status',
                'regular_price',
                'sale_price',
                'stock_status',
                'stock',
                'manage_stock',
                'Images',
                'tax:product_type',
                'tax:product_cat',
                'tax:product_tag',
            ];
        }

        return $heading;
    }

    public function array(): array
    {
        $data = [];
        $query = WooProduct::where('woo_site_id', self::WOO_SITE_ID)
            ->where('is_checked', 0)
            ->limit($this->limitRow);

        if (self::FILTER_CATEGORY !== 'all') {
            $query->where('category', self::FILTER_CATEGORY);
        }
        $collection = $query->get();

        foreach ($collection as $item) {
            $parentSku = $this->generateSku('CAP');

            if (self::PRICE_DISCOUNT_PERCENT === 0) {
                $itemRegularPrice = $this->setSalePrice($item->price);
                $itemSalePrice = '';
            } else {
                $itemSalePrice = $this->setSalePrice($item->price);
                $itemRegularPrice = ceil($itemSalePrice / (1 - (self::PRICE_DISCOUNT_PERCENT / 100)));
            }

            // Size.
            $sizeArray = [];
            if (!is_null($item->size)) {
                $sizeArray = explode('|', $item->size);
            }

            /** Variation products. */
            if (self::PRODUCT_TYPE === 'variation') {
                $data[] = [
                    'parent_sku' => '',
                    'sku' => $parentSku,
                    'meta:attribute_size' => '',
                    'attribute:Size' => $item->size,
                    'attribute_data:Size' => '1|1|1',
                    'attribute_default:Size' => $sizeArray[0],
                    'post_title' => trim($item->title),
                    'post_excerpt' => trim($item->short_desc),
                    'post_content' => trim($item->description),
                    'post_status' => 'publish',
                    'regular_price' => '',
                    'sale_price' => '',
                    'stock_status' => '',
                    'stock' => '',
                    'manage_stock' => '',
                    'Images' => $item->images,
                    'tax:product_type' => 'variable',
                    'tax:product_cat' => self::CATEGORY_OUTPUT,
                    'tax:product_tag' => $item->tags,
                ];

                foreach ($sizeArray as $size) {
                    $data[] = [
                        'parent_sku' => $parentSku,
                        'sku' => $parentSku . '-' . $size,
                        'meta:attribute_size' => $size,
                        'attribute:Size' => '',
                        'attribute_data:Size' => '',
                        'attribute_default:Size' => '',
                        'post_title' => '',
                        'post_excerpt' => '',
                        'post_content' => '',
                        'post_status' => 'publish',
                        'regular_price' => $itemRegularPrice,
                        'sale_price' => $itemSalePrice,
                        'stock_status' => 'instock',
                        'stock' => '999',
                        'manage_stock' => 'no',
                        'Images' => '',
                        'tax:product_type' => '',
                        'tax:product_cat' => '',
                        'tax:product_tag' => '',
                    ];
                }
            }
            /** eof variation products. */

            /** Simple products. */
            if (self::PRODUCT_TYPE === 'simple') {
                $data[] = [
                    'sku' => $this->generateSku('JON'),
                    'post_title' => trim($item->title),
                    'post_excerpt' => trim($item->short_desc),
                    'post_content' => trim($item->description),
                    'post_status' => 'publish',
                    'regular_price' => $itemRegularPrice,
                    'sale_price' => $itemSalePrice,
                    'stock_status' => '',
                    'stock' => '',
                    'manage_stock' => '',
                    'Images' => $item->images,
                    'tax:product_type' => 'simple',
                    'tax:product_cat' => self::CATEGORY_OUTPUT,
                    'tax:product_tag' => $item->tags,
                ];
            }
            /** eof simple products. */

            $item->update(['is_checked' => 1]);
        }

        return $data;
    }

    public function map($row): array
    {
        if (self::PRODUCT_TYPE === 'simple') {
            $dataArray = [
                $row['sku'],
                $row['post_title'],
                $row['post_excerpt'],
                $row['post_content'],
                $row['post_status'],
                $row['regular_price'],
                $row['sale_price'],
                $row['stock_status'],
                $row['stock'],
                $row['manage_stock'],
                $row['Images'],
                $row['tax:product_type'],
                $row['tax:product_cat'],
                $row['tax:product_tag'],
            ];
        } elseif (self::PRODUCT_TYPE === 'variation') {
            $dataArray = [
                $row['parent_sku'],
                $row['sku'],
                $row['meta:attribute_size'],
                $row['attribute:Size'],
                $row['attribute_data:Size'],
                $row['attribute_default:Size'],
                $row['post_title'],
                $row['post_excerpt'],
                $row['post_content'],
                $row['post_status'],
                $row['regular_price'],
                $row['sale_price'],
                $row['stock_status'],
                $row['stock'],
                $row['manage_stock'],
                $row['Images'],
                $row['tax:product_type'],
                $row['tax:product_cat'],
                $row['tax:product_tag'],
            ];
        }

        return $dataArray;
    }

    private function generateSku($prefix = 'MTA')
    {
        return $prefix . '-' . date('ymd') . rand(10000000, 99999999);
    }

    private function setSalePrice($price)
    {
        if (self::DECREASE_AMOUNT_PRICE > 0) {
            $updatePrice = $price - self::DECREASE_AMOUNT_PRICE;
        } else {
            if ($price < 80) {
                $discountPercent = self::DECREASE_PERCENT_PRICE_LOWER_80;
            } elseif ($price >= 80 and $price < 120) {
                $discountPercent = self::DECREASE_PERCENT_PRICE_LOWER_120;
            } else {
                $discountPercent = self::DECREASE_PERCENT_PRICE_HIGHER_120;
            }
            $updatePrice = intval($price * (1 - ($discountPercent / 100))) + 0.99;
        }

        return $updatePrice;
    }
}



























