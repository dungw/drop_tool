<?php

namespace App\Exports;

use App\Helpers\CrawlProductHelper;
use App\Models\CrawlProduct;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class CrawlProductExport implements FromArray, WithMapping, WithHeadings
{
    use Exportable;

    const LIMIT_PER_FILE = 1000;
    private $limitRow;

    public function __construct($limitRow)
    {
        $this->limitRow = $limitRow;
    }

    public function headings(): array
    {
        return [
            'parent_sku',
            'sku',
            'post_title',
            'post_excerpt',
            'post_content',
            'post_status',
            'regular_price',
            'sale_price',
            'stock_status',
            'stock',
            'manage_stock',
            'Images',
            'tax:product_type',
            'tax:product_cat',
            'tax:product_tag',
            'meta:attribute_size',
            'attribute:size',
            'attribute_data:size',
            'attribute_default:size',
        ];
    }

    public function array(): array
    {
        $data = [];
        $collection = CrawlProduct::where('is_exported', 0)
            ->whereNotNull('manufacture_sku')
            ->where('price', '>=', 120)
            ->limit(self::LIMIT_PER_FILE)
            ->get();

        foreach ($collection as $item) {
            $productSizeList = $item->gender === CrawlProduct::GENDER_MENS ? CrawlProduct::MENS_SHOES_SIZE_LIST : CrawlProduct::WOMENS_SHOES_SIZE_LIST;
            $sizeListArray = explode('|', $productSizeList);

            $itemSku = CrawlProductHelper::generateSku('shoes', $item->brand, $item->manufacture_sku);
            $itemSalePrice = CrawlProductHelper::setSalePrice($item->price);
            $itemRegularPrice = $item->price;

            $data[] = [
                'parent_sku' => null,
                'sku' => $itemSku,
                'post_title' => CrawlProductHelper::generateName($item),
                'post_excerpt' => $item->short_description,
                'post_content' => CrawlProductHelper::cleanDescription($item->description),
                'post_status' => 'publish',
                'regular_price' => '',
                'sale_price' => '',
                'stock_status' => '',
                'stock' => '',
                'manage_stock' => '',
                'Images' => $item->images,
                'tax:product_type' => 'variable',
                'tax:product_cat' => 'New Arrivals',
                'tax:product_tag' => CrawlProductHelper::generateProductTag($item),
                'meta:attribute_size' => '',
                'attribute:size' => $productSizeList,
                'attribute_data:size' => '0|1|1',
                'attribute_default:size' => '',
            ];

            foreach ($sizeListArray as $k => $sizeItem) {
                $data[] = [
                    'parent_sku' => $itemSku,
                    'sku' => $itemSku . '-' . $k,
                    'post_title' => '',
                    'post_excerpt' => '',
                    'post_content' => '',
                    'post_status' => 'publish',
                    'regular_price' => $itemRegularPrice,
                    'sale_price' => '',
                    'stock_status' => 'instock',
                    'stock' => rand(10, 20),
                    'manage_stock' => 'no',
                    'Images' => '',
                    'tax:product_type' => '',
                    'tax:product_cat' => '',
                    'tax:product_tag' => '',
                    'meta:attribute_size' => $sizeItem,
                    'attribute:size' => '',
                    'attribute_data:size' => '',
                    'attribute_default:size' => '',
                ];
            }

            $item->update(
                ['is_exported' => 1]
            );
        }

        return $data;
    }

    public function map($row): array
    {
        return [
            $row['parent_sku'],
            $row['sku'],
            $row['post_title'],
            $row['post_excerpt'],
            $row['post_content'],
            $row['post_status'],
            $row['regular_price'],
            $row['sale_price'],
            $row['stock_status'],
            $row['stock'],
            $row['manage_stock'],
            $row['Images'],
            $row['tax:product_type'],
            $row['tax:product_cat'],
            $row['tax:product_tag'],
            $row['meta:attribute_size'],
            $row['attribute:size'],
            $row['attribute_data:size'],
            $row['attribute_default:size'],
        ];
    }
}



























