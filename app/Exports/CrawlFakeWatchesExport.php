<?php

namespace App\Exports;

use App\Helpers\CrawlProductHelper;
use App\Models\CrawlFakeWatches;
use App\Models\CrawlProduct;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class CrawlFakeWatchesExport implements FromArray, WithMapping, WithHeadings
{
    use Exportable;

    const LIMIT_PER_FILE = 10000;
    private $limitRow;

    public function __construct($limitRow)
    {
        $this->limitRow = $limitRow;
    }

    public function headings(): array
    {
        return [
            'name',
            'url',
            'image',
            'model_number',
            'price',
        ];
    }

    public function array(): array
    {
        $data = [];
        $collection = CrawlFakeWatches::select(['crawl_fake_watches.name', 'crawl_fake_watches.model_number', 'crawl_fake_watches.images', 'crawl_links.url', 'crawl_links.price'])
            ->leftJoin('crawl_links', 'crawl_fake_watches.crawl_link_id', '=', 'crawl_links.id')
            ->limit(self::LIMIT_PER_FILE)
            ->get();

        foreach ($collection as $item) {
            $data[] = [
                'name' => $item->name,
                'url' => $item->url,
                'image' => $item->images,
                'model_number' => $item->model_number,
                'price' => $item->price,
            ];
        }

        return $data;
    }

    public function map($row): array
    {
        return [
            $row['name'],
            $row['url'],
            $row['image'],
            $row['model_number'],
            $row['price'],
        ];
    }
}



























