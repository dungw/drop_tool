<?php

namespace App\Helpers;

use App\Models\CrawlProduct;
use Sunra\PhpSimple\HtmlDomParser;

class CrawlProductHelper
{
    const BRAND_CODES = [
        'nike' => '01',
        'jordan' => '02',
        'converse' => '03',
        'adidas' => '04',
        'asics' => '05',
        'ewing' => '06',
        'new balance' => '07',
        'puma' => '08',
        'reebok' => '09',
        'saucony' => '10',
        'timberland' => '11',
        'vans' => '12',
    ];
    const OTHER_BRAND_CODE = '99';

    public static function get_domain($url)
    {
        $pieces = parse_url($url);
        $domain = isset($pieces['host']) ? $pieces['host'] : $pieces['path'];

        if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
            return $regs['domain'];
        }

        return false;
    }

    public static function generateSku($productCategory = 'shoes', $brand = '', $manufactureSku = '')
    {
        $categoryCode = '';
        if ($productCategory === 'shoes') {
            $categoryCode = 'S';
        }

        $brand = strtolower($brand);
        $rightBrandCode = '';
        foreach (self::BRAND_CODES as $brandName => $brandCode) {
            if (strpos($brand, $brandName) !== false) {
                $rightBrandCode = $brandCode;
            }
        }
        if ($rightBrandCode === '') {
            $rightBrandCode = self::OTHER_BRAND_CODE;
        }

        if (trim($manufactureSku) === '') {
            $manufactureSku = rand(100000, 999999);
        };

        return $categoryCode . $rightBrandCode . '-' . $manufactureSku;;
    }

    public static function setSalePrice($regularPrice)
    {
        $discountValue = 0;

        if ($regularPrice >= 120 && $regularPrice <= 200) {
            $discountPercent = rand(25, 30);
            $discountValue = $regularPrice * ($discountPercent / 100);

        } elseif ($regularPrice > 200 && $regularPrice <= 250) {
            $discountPercent = rand(33, 35);
            $discountValue = $regularPrice * ($discountPercent / 100);
        } elseif ($regularPrice > 250 && $regularPrice <= 300) {

            $discountPercent = rand(43, 45);
            $discountValue = $regularPrice * ($discountPercent / 100);
        } elseif ($regularPrice > 300 && $regularPrice <= 500) {

            $discountPercent = rand(53, 55);
            $discountValue = $regularPrice * ($discountPercent / 100);
        } elseif ($regularPrice > 500) {

            $discountPercent = rand(58, 60);
            $discountValue = $regularPrice * ($discountPercent / 100);
        }

        return intval($regularPrice - $discountValue);
    }

    public static function generateProductTag(CrawlProduct $product)
    {

        return $product->name;
    }

    public static function generateName(CrawlProduct $product)
    {
        $brand = $product->brand;
        $name = $product->name;

        if (trim($brand) !== '' && strpos($name, $brand) === false) {
            $name = $brand . ' ' . $name;
        }

        return $name;
    }

    public static function cleanDescription($description)
    {
        $description = str_replace('Be the first to review this product', '', $description);
        $description = str_replace('https://www.stadiumgoods.com/', '', $description);
        $description = str_replace('<h4>Details</h4>', '', $description);
        $description = str_replace('<div class="info-content">', '<br><br><div class="info-content">', $description);

        return $description;
    }
}



























