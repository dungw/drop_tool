<?php namespace App\Helpers\Crawler;

use App\Helpers\Crawler\phpWebHacks;
use Sunra\PhpSimple\HtmlDomParser;

class HtmlParser
{
    /**
     * Get html from url.
     */
	public static function getHTML($url, $return_html = 0, $port = 80)
	{
		$browser = new phpWebHacks($port);
		if ($port != 80)
		{
			$browser->changePort($port);
		}

		$response = $browser->get($url);
		if ($return_html == 0)
		{
			return HtmlDomParser::str_get_html($response);
		}

		return $response;
	}

	/**
     * Decorate strings.
     */
	public static function decoStringField($value)
	{
		return ('"' . addslashes(self::remove_spaces(trim($value))) . '"');
	}

    /**
     * Remove spaces from string.
     *
     * @param $str
     * @return mixed
     */
	public static function removeSpaces($str)
	{
		$str = preg_replace('/(&nbsp;)+/', ' ', $str);
		return preg_replace('/\s+/', ' ', $str);
	}
}
