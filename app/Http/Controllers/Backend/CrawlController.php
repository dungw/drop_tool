<?php

namespace App\Http\Controllers\Backend;

use App\Exports\CrawlFakeWatchesExport;
use App\Exports\CrawlWooProductExport;
use App\Exports\SzwegoProductExport;
use App\Helpers\Crawler\HtmlParser;
use App\Http\Controllers\Controller;
use App\Models\BrandPortCategory;
use App\Models\CrawlFakeWatches;
use App\Models\CrawlLink;
use App\Models\CrawlPage;
use App\Models\SzwegoProduct;
use App\Models\WooProduct;
use App\Models\WooSite;
use Maatwebsite\Excel\Excel;

class CrawlController extends Controller
{
    public function index()
    {
        return $this->exportCrawlWooProduct();
    }

    /** Get products of brandporterms.ru */
    public function getBrandPortProduct()
    {

    }

    /** Get categories of brandporterms.ru */
    public function getBrandPortCategory()
    {
        /** CONFIG **/
        $cateArray = [
            [
                'url' => 'https://www.brandporterms.ru/index.php?route=product/category&path=194',
                'level_number' => 2,
                'parent_id' => 1,
            ],
            [
                'url' => 'https://www.brandporterms.ru/index.php?route=product/category&path=327',
                'level_number' => 2,
                'parent_id' => 2,
            ],
            [
                'url' => 'https://www.brandporterms.ru/index.php?route=product/category&path=263',
                'level_number' => 2,
                'parent_id' => 4,
            ],
            [
                'url' => 'https://www.brandporterms.ru/index.php?route=product/category&path=770',
                'level_number' => 2,
                'parent_id' => 5,
            ],
            [
                'url' => 'https://www.brandporterms.ru/index.php?route=product/category&path=435_902',
                'level_number' => 3,
                'parent_id' => 3,
            ],
            [
                'url' => 'https://www.brandporterms.ru/index.php?route=product/category&path=435_903',
                'level_number' => 3,
                'parent_id' => 3,
            ],
            [
                'url' => 'https://www.brandporterms.ru/index.php?route=product/category&path=435_904',
                'level_number' => 3,
                'parent_id' => 3,
            ],
            [
                'url' => 'https://www.brandporterms.ru/index.php?route=product/category&path=435_905',
                'level_number' => 3,
                'parent_id' => 3,
            ],
            [
                'url' => 'https://www.brandporterms.ru/index.php?route=product/category&path=435_906',
                'level_number' => 3,
                'parent_id' => 3,
            ],
        ];
        /** EOF CONFIG **/

        foreach ($cateArray as $cate) {
            $url = $cate['url'];
            $levelNumber = $cate['level_number'];
            $parentId = $cate['parent_id'];
            $html = HtmlParser::getHTML($url);
            $children = [];

            /**
             * This is 3 levels.
             */
            if ($levelNumber === 3) {
                $parentLi = $html->find('div[class="box-content box-category"] ul li[class="open active]', 1);
                $parentA = $parentLi->find('a', 0);
                $parentName = trim($parentA->plaintext);
                $parentCate = BrandPortCategory::create(
                    [
                        'name' => $parentName,
                        'parent_id' => $parentId,
                        'url' => $this->formatUrl($parentA->href),
                        'level' => 2,
                    ]
                );

                foreach ($parentLi->find('ul li a') as $childLi) {
                    $children[] = [
                        'name' => trim($childLi->plaintext),
                        'parent_id' => $parentCate->id,
                        'url' => $this->formatUrl($childLi->href),
                        'level' => 3,
                    ];
                }
            } /**
             * This is 2 levels.
             */
            elseif ($levelNumber === 2) {
                $parentLi = $html->find('div[class="box-content box-category"] ul li[class="open active]', 0);
                foreach ($parentLi->find('ul li a') as $childLi) {
                    $children[] = [
                        'name' => trim($childLi->plaintext),
                        'parent_id' => $parentId,
                        'url' => $this->formatUrl($childLi->href),
                        'level' => 3,
                    ];
                }
            }

            BrandPortCategory::insert($children);
        }
    }

    private function formatUrl($url)
    {
        $url = trim($url);
        $url = str_replace('&amp;', '&', $url);

        return $url;
    }

    public static function cleanChineseTitle($title)
    {
        $removableChars = [
            '➕',
            '。',
            '！',
            '，',
            '：',
            '/',
            '#',
        ];

        $title = preg_replace("/\p{Han}+/u", '', $title);

        foreach ($removableChars as $char) {
            $title = str_replace($char, ' ', $title);
        }

        $title = str_replace('  ', ' ', $title);
        $title = str_replace('   ', ' ', $title);

        return $title;
    }

    public function exportCrawlWooProduct()
    {
        $limitRow = 500;
        $exporter = new CrawlWooProductExport($limitRow);
        return ($exporter)->download($limitRow . '_crawl_woo_product_'. rand(1000000, 9999999) .'.csv', Excel::CSV);
    }

    public function getWooProduct(WooProduct $wooProduct)
    {
        $url = $wooProduct->url;
        dump($url);
        $html = HtmlParser::getHTML($url);
        $domain = $wooProduct->wooSite->url;
        $domain = str_replace('https://', '', $domain);
        $domain = str_replace('http://', '', $domain);

        // get category.
        $category = '';
        $cateDom = $html->find('nav[class="woocommerce-breadcrumb breadcrumbs uppercase"] a');
        if (!is_null($cateDom) && count($cateDom) > 1) {
            $category = trim($cateDom[1]->plaintext);
        }

        $description = $html->find('div[id="tab-description"]', 0);
        if (!is_null($description)) {
            $description = $description->innertext();
            $description = str_replace($domain, '', $description);
        } else {
            $description = $wooProduct->title;
        }

        $images = [];
        $imageTags = $html->find('div[class^="woocommerce-product-gallery__image slide"] img');
        foreach ($imageTags as $imageTag) {
            $width = intval($imageTag->getAttribute('width'));
            if ($width < 300) {
                continue;
            }
            $images[] = trim($imageTag->getAttribute('src'));
        }
        $images = array_unique($images);

        // get size
        $sizes = [];
        $sizeDom = $html->find('select[id="pa_size"]', 0);
        if (!is_null($sizeDom)) {
            $sizeOptions = $sizeDom->find('option');
            foreach ($sizeOptions as $sizeLi) {
                $optionValue = trim($sizeLi->plaintext);
                if ($optionValue === 'Choose an option') {
                    continue;
                }
                $sizes[] = $optionValue;
            }
        }

        // get color.
        $colors = [];
        $colorDom = $html->find('select[id="pa_color"]', 0);
        if (!is_null($colorDom)) {
            $colorOptions = $colorDom->find('option');
            foreach ($colorOptions as $colorOption) {
                $colorOptionValue = trim($colorOption->plaintext);
                if ($colorOptionValue === 'Choose an option') {
                    continue;
                }
                $colors[] = $colorOptionValue;
            }
        }

        $wooProductData = [
            'images' => implode('|', $images),
            'description' => $description,
            'category' => $category,
            'color' => implode('|', $colors),
            'size' => implode('|', $sizes),
        ];

        $wooProduct->update($wooProductData);
    }

    public function getCurrentJoyDetail(WooProduct $wooProduct)
    {
        dump($wooProduct->url);
        $url = $wooProduct->url;
        $html = HtmlParser::getHTML($url);

        $domain = $wooProduct->wooSite->url;
        $domain = str_replace('https://', '', $domain);
        $domain = str_replace('http://', '', $domain);

        $description = $html->find('div[id="tab-description"]', 0);
        if (!is_null($description)) {
            $description = $description->innertext();
            $description = str_replace($domain, '', $description);
        } else {
            $description = $wooProduct->title;
        }

        return $wooProduct->update(['description' => $description]);
    }

    public function getMlbJerseyDetail(WooProduct $wooProduct)
    {
        $url = $wooProduct->url;
        dump($url);
        $html = HtmlParser::getHTML($url);
        $restrictWords = [
            'officially',
            'licensed',
            'made in',
            'brand',
            'replica',
            'nike',
            'authentic',
            'material',
            'legacy'
        ];

        $categories = [];
        $categoryLis = $html->find('div[class="mlb-shop-breadcrumb"] ul li');
        if (!is_null($categoryLis)) {
            foreach ($categoryLis as $li) {
                $liContent = trim($li->plaintext);
                $liContentLower = strtolower($liContent);
                $willGet = true;
                foreach ($restrictWords as $restrictWord) {
                    if (strpos($liContentLower, $restrictWord) !== false) {
                        $willGet = false;
                        break;
                    }
                }

                if ($willGet === true) {
                    $categories[] = $liContent;
                }
            }
        }
        $categoryString = implode('>', $categories);

        $desc = $html->find('div[itemprop="description"]', 0);
        if (is_null($desc)) {
            $descContent = '<ul><li>Material: 100% Polyester</li><li>Screen print graphics</li><li>Tagless collar</li><li>Embroidered fabric applique</li><li>Machine wash</li><li>Imported</li></ul>';
        } else {
            $descUl = $desc->find('ul', 0);
            if (is_null($descUl)) {
                $descContent = '<ul><li>Material: 100% Polyester</li><li>Screen print graphics</li><li>Tagless collar</li><li>Embroidered fabric applique</li><li>Machine wash</li><li>Imported</li></ul>';
            } else {
                $descContentArr = [];
                foreach ($descUl->find('li') as $li) {
                    $liContent = strtolower(trim($li->plaintext));
                    $canGet = true;
                    foreach ($restrictWords as $restrictWord) {
                        if (strpos($liContent, $restrictWord) !== false) {
                            $canGet = false;
                            break;
                        }
                    }

                    if ($canGet === true) {
                        $descContentArr[] = '<li>' . $li->plaintext . '</li>';;
                    }
                }
                $descContent = '<ul>'. implode('', $descContentArr) .'</ul>';
            }
        }

        $descContent = str_replace('Brand: Nike', '', $descContent);
        $descContent = str_replace('Officially licensed', '', $descContent);

        // Clean title.
        $cleanWords = [
            'Authentic',
            'Nike',
            'Replica',
            'Brand',
            'Legacy',
        ];

        $title = $wooProduct->title;
        foreach ($cleanWords as $cleanWord) {
            if (strpos($title, $cleanWord) !== false) {
                $title = str_replace($cleanWord, '', $title);
            }
        }

        $wooProductData = [
            'category' => $categoryString,
            'description' => $descContent,
            'title' => $title,
        ];

        $wooProduct->update($wooProductData);
    }

    public function getMlbJerseyPage(WooSite $wooSite, $getPage)
    {
        $url = $wooSite->url . "-s-0-0-0-0-0-0-0-{$getPage}";

        $html = HtmlParser::getHTML($url);
        $ul = $html->find('ul[class="mlb-shop-products"] li');
        $wooProduct = [];

        foreach ($ul as $li) {
            $name = trim($li->find('p', 0)->plaintext);
            $name = str_replace('Authentic', '', $name);
            $url = "https://www.mlbjersey.online" . trim($li->find('a', 0)->href);
            $img = "https://www.mlbjersey.online" . trim($li->find('img', 0)->src);
            $category = 'MLB Jersey';
            $price = (trim($li->find('span[class="price"]', 0)->plaintext));
            $price = str_replace('$', '', $price);

            $wooProduct[] = [
                'woo_site_id' => $wooSite->id,
                'url' => $url,
                'price' => $price,
                'title' => $name,
                'page_number' => $getPage,
                'category' => $category,
                'images' => $img,
                'size' => 'S|M|L|XL|2XL|3XL',
            ];
        }

        WooProduct::insert($wooProduct);

        if ($getPage === 1) {
            $allPage = [];
            $pagination = $html->find('div[id="showpage"] a');
            foreach ($pagination as $page) {
                $allPage[] = intval($page->plaintext);
            }

            return [
                'total_page' => max($allPage)
            ];
        }

        return true;
    }

    public function getNbaOnlineJerseysShop(WooSite $wooSite, $getPage)
    {
        $url = $wooSite->url . "{$getPage}";

        $html = HtmlParser::getHTML($url);
        $ul = $html->find('ul[class="products"] li');
        $wooProduct = [];

        foreach ($ul as $li) {
            $name = trim($li->find('p', 0)->plaintext);
            $name = str_replace('Authentic', '', $name);
            $url = "https://www.nbaonlinejerseys.shop" . trim($li->find('a', 0)->href);
            $img = "https://www.nbaonlinejerseys.shop" . trim($li->find('img', 0)->src);
            $category = 'NBA';
            $price = (trim($li->find('span[class="price"]', 0)->plaintext));
            $price = str_replace('$', '', $price);

            $wooProduct[] = [
                'woo_site_id' => $wooSite->id,
                'url' => $url,
                'price' => $price,
                'title' => $name,
                'page_number' => $getPage,
                'category' => $category,
                'images' => $img,
                'size' => 'S|M|L|XL|2XL|3XL',
            ];
        }

        WooProduct::insert($wooProduct);

        if ($getPage === 1) {
            $allPage = [];
            $pagination = $html->find('div[id="showpage"] a');
            foreach ($pagination as $page) {
                $allPage[] = intval($page->plaintext);
            }

            return [
                'total_page' => max($allPage)
            ];
        }

        return true;
    }

    public function getNbaOnlineJerseysShopDetail(WooProduct $wooProduct)
    {
        $url = $wooProduct->url;
        dump($url);
        $html = HtmlParser::getHTML($url);
        $restrictWords = [
            'officially',
            'licensed',
            'made in',
            'brand',
            'replica',
            'nike',
            'authentic',
            'material',
            'legacy'
        ];

        $categories = [];
        $categoryLis = $html->find('div[class="breadcrumb"] ul li');
        if (!is_null($categoryLis)) {
            foreach ($categoryLis as $li) {
                $liContent = trim($li->plaintext);
                $categories[] = $liContent;
            }
        }
        $categoryString = $categories[count($categories)-1];

        $desc = $html->find('div[itemprop="description"]', 0);
        if (is_null($desc)) {
            $descContent = '<ul><li>Material: 100% Polyester</li><li>Screen print graphics</li><li>Tagless collar</li><li>Embroidered fabric applique</li><li>Machine wash</li><li>Imported</li></ul>';
        } else {
            $descUl = $desc->find('ul', 0);
            if (is_null($descUl)) {
                $descContent = '<ul><li>Material: 100% Polyester</li><li>Screen print graphics</li><li>Tagless collar</li><li>Embroidered fabric applique</li><li>Machine wash</li><li>Imported</li></ul>';
            } else {
                $descContentArr = [];
                foreach ($descUl->find('li') as $li) {
                    $liContent = strtolower(trim($li->plaintext));
                    $canGet = true;
                    foreach ($restrictWords as $restrictWord) {
                        if (strpos($liContent, $restrictWord) !== false) {
                            $canGet = false;
                            break;
                        }
                    }

                    if ($canGet === true) {
                        $descContentArr[] = '<li>' . $li->plaintext . '</li>';;
                    }
                }
                $descContent = '<ul>' . implode('', $descContentArr) . '</ul>';
            }
        }

        $descContent = str_replace('Brand: Nike', '', $descContent);
        $descContent = str_replace('Officially licensed', '', $descContent);

        // Clean title.
        $cleanWords = [
            'Authentic',
            'Nike',
            'Replica',
            'Brand',
            'Legacy',
        ];

        $title = $wooProduct->title;
        foreach ($cleanWords as $cleanWord) {
            if (strpos($title, $cleanWord) !== false) {
                $title = str_replace($cleanWord, '', $title);
            }
        }

        $wooProductData = [
            'category' => $categoryString,
            'description' => $descContent,
            'title' => $title,
        ];

        $wooProduct->update($wooProductData);
    }

    public function getNcaaStore(WooSite $wooSite, $getPage)
    {
        $url = $wooSite->url . "{$getPage}";

        $html = HtmlParser::getHTML($url);
        $ul = $html->find('ul[class="ncaa-products"] li');
        $wooProduct = [];

        foreach ($ul as $li) {
            $name = trim($li->find('p', 0)->plaintext);
            $name = str_replace('Authentic', '', $name);
            $url = "https://www.ncaajerseys.store" . trim($li->find('a', 0)->href);
            $img = "https://www.ncaajerseys.store" . trim($li->find('img', 0)->src);
            $category = 'NCAA';
            $price = (trim($li->find('span[class="price"]', 0)->plaintext));
            $price = str_replace('$', '', $price);

            $wooProduct[] = [
                'woo_site_id' => $wooSite->id,
                'url' => $url,
                'price' => $price,
                'title' => $name,
                'page_number' => $getPage,
                'category' => $category,
                'images' => $img,
                'size' => 'S|M|L|XL|2XL|3XL',
            ];
        }

        WooProduct::insert($wooProduct);

        if ($getPage === 1) {
            $allPage = [];
            $pagination = $html->find('div[id="showpage"] a');
            foreach ($pagination as $page) {
                $allPage[] = intval($page->plaintext);
            }

            return [
                'total_page' => max($allPage)
            ];
        }

        return true;
    }

    public function getNcaaStoreDetail(WooProduct $wooProduct)
    {
        $url = $wooProduct->url;
        dump($url);
        $html = HtmlParser::getHTML($url);
        $restrictWords = [
            'officially',
            'licensed',
            'made in',
            'brand',
            'replica',
            'nike',
            'authentic',
            'material',
            'legacy'
        ];

        $categories = [];
        $categoryLis = $html->find('div[class="ncaa-breadcrumb"] ul li');
        if (!is_null($categoryLis)) {
            foreach ($categoryLis as $li) {
                $liContent = trim($li->plaintext);
                $categories[] = $liContent;
            }
        }
        $categoryString = $categories[count($categories) - 1];

        $desc = $html->find('div[itemprop="description"]', 0);
        if (is_null($desc)) {
            $descContent = '<ul><li>Material: 100% Polyester</li><li>Screen print graphics</li><li>Tagless collar</li><li>Embroidered fabric applique</li><li>Machine wash</li><li>Imported</li></ul>';
        } else {
            $descUl = $desc->find('ul', 0);
            if (is_null($descUl)) {
                $descContent = '<ul><li>Material: 100% Polyester</li><li>Screen print graphics</li><li>Tagless collar</li><li>Embroidered fabric applique</li><li>Machine wash</li><li>Imported</li></ul>';
            } else {
                $descContentArr = [];
                foreach ($descUl->find('li') as $li) {
                    $liContent = strtolower(trim($li->plaintext));
                    $canGet = true;
                    foreach ($restrictWords as $restrictWord) {
                        if (strpos($liContent, $restrictWord) !== false) {
                            $canGet = false;
                            break;
                        }
                    }

                    if ($canGet === true) {
                        $descContentArr[] = '<li>' . $li->plaintext . '</li>';;
                    }
                }
                $descContent = '<ul>' . implode('', $descContentArr) . '</ul>';
            }
        }

        $descContent = str_replace('Brand: Nike', '', $descContent);
        $descContent = str_replace('Officially licensed', '', $descContent);

        // Clean title.
        $cleanWords = [
            'Authentic',
            'Nike',
            'Replica',
            'Brand',
            'Legacy',
        ];

        $title = $wooProduct->title;
        foreach ($cleanWords as $cleanWord) {
            if (strpos($title, $cleanWord) !== false) {
                $title = str_replace($cleanWord, '', $title);
            }
        }

        $wooProductData = [
            'category' => $categoryString,
            'description' => $descContent,
            'title' => $title,
        ];

        $wooProduct->update($wooProductData);
    }

    /**
     * @param WooSite $wooSite
     * @param $getPage
     * @return array|bool
     */
    public function getNflShopPage(WooSite $wooSite, $getPage)
    {
        $url = $wooSite->url . "?pageSize=24&pageNumber={$getPage}&sortOption=TopSellers";
        dump($url);
        $html = HtmlParser::getHTML($url);
        print $html;die;
        $containerDivs = $html->find('div[class^="product-card"]');
        $wooProduct = [];
dump(count($containerDivs));
        foreach ($containerDivs as $containerDiv) {

            // Get image.
            $fullImageUrl = '';
            $imageSrc = trim($containerDiv->find('div[class="card-image-container"] img', 0)->src);
            dump($imageSrc);
            if (strpos($imageSrc, '&w=') !== false) {
                $temp = explode('&w=', $imageSrc);
                $fullImageUrl = 'https:' . $temp[0] . '&w=1000';
            }
dump($fullImageUrl);
            $aTag = $containerDiv->find('h4[class="product-card-title"] a', 0);
            if (substr($aTag->href, 0, 1) === '/') {
                $url = 'https://www.nflshop.com' . $aTag->href;
            } else {
                $url = 'https://www.nflshop.com/' . $aTag->href;
            }

            $name = trim($aTag->plaintext);

            $category = 'NFL';
            $price = (trim($containerDiv->find('span[class="regular-price"]', 0)->plaintext));
            $price = str_replace('$', '', $price);

            $wooProduct[] = [
                'woo_site_id' => $wooSite->id,
                'url' => $url,
                'price' => $price,
                'title' => $name,
                'page_number' => $getPage,
                'category' => $category,
                'images' => $fullImageUrl,
                'size' => 'S|M|L|XL|2XL|3XL',
            ];
        }
dd($wooProduct);
        WooProduct::insert($wooProduct);

        if ($getPage === 1) {
            $allPage = [];
            $pagination = $html->find('div[id="showpage"] a');
            foreach ($pagination as $page) {
                $allPage[] = intval($page->plaintext);
            }

            return [
                'total_page' => max($allPage)
            ];
        }

        return true;
    }

    public function getWooSitePage(WooSite $wooSite, $getPage)
    {
        $url = $wooSite->url . "/shop/page/{$getPage}/";
        if ($wooSite->no_shop_page === 1) {
            $url = $wooSite->url . "page/{$getPage}/";
        }

        $html = HtmlParser::getHTML($url);
        $items = $html->find('div[class="product-small box"]');

        $wooProduct = [];
        foreach ($items as $item) {
            $productUrl = trim($item->find('a', 0)->href);
            $productTitle = trim($item->find('p[class="name product-title"]', 0)->plaintext);
            $productPrice = $item->find('span[class="woocommerce-Price-amount amount"]', 0);

            if (is_null($productPrice)) {
                continue;
            }   
            $productPrice = trim($productPrice->plaintext);
            $productPrice = trim(str_replace(',', '', $productPrice));
            $productPrice = floatval(trim(str_replace('&#36;', '', $productPrice)));

            // get category.
            $category = '';
            $cateDom = $html->find('p[class*="category uppercase"]', 0);
            if (!is_null($cateDom)) {
                $category = trim($cateDom->plaintext);
            }

            // Get images.
            $images = '';
            if ($wooSite->only_first_image === 1) {
                $imageSrc = trim($item->find('img', 0)->src);
                dump($imageSrc);
                $tmp = explode('-', $imageSrc);
                $tmp1 = explode('.', $tmp[count($tmp) - 1]);
                unset($tmp[count($tmp) - 1]);
                $images = implode('-', $tmp) . '.' . $tmp1[1];;
            }

            $wooProduct[] = [
                'woo_site_id' => $wooSite->id,
                'url' => $productUrl,
                'price' => $productPrice,
                'title' => $productTitle,
                'page_number' => $getPage,
                'category' => $category,
                'images' => $images,
            ];
        }

        WooProduct::insert($wooProduct);

        if ($getPage === 1) {
            $allPage = [];
            $pagination = $html->find('nav[class="woocommerce-pagination"] li a[class="page-number"]');
            foreach ($pagination as $page) {
                $allPage[] = intval($page->plaintext);
            }

            return [
                'total_page' => max($allPage)
            ];
        }

        return true;
    }

    public function exportSzwegoProduct()
    {
        $exporter = new SzwegoProductExport();
        return ($exporter)->download('szwego.csv', Excel::CSV);
    }

    public function getSzwegoByPage($shopId, $shopUrl, $page)
    {
        $shopUrl .= '&page_index=' . $page;
        dump($shopUrl);
        $returnData = [];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $shopUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $headers = [
            'Sec-Fetch-Mode: cors',
            'Sec-Fetch-Site: same-origin',
            'Accept-Encoding: gzip, deflate, br',
            'Accept-Language: en,en-US;q=0.9,vi;q=0.8,en-CA;q=0.7,da;q=0.6',
            'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36',
            'Accept: application/json, text/javascript, */*; q=0.01',
            'Referer: https://www.szwego.com/static/index.html?t=1574743719522',
            'X-Requested-With: XMLHttpRequest',
            'Cookie: token=RDk0RjZFRjkyQTgzRUEwRDE0MUFFNDUxQTBGMEIwQUI4NEZCM0JBRDIxOEVFMDYxQTgzQUY3MTM3RUQxOUYwQTRBMDgyQzE2MjEzMTI0MTQzMkVGMEJGNDc2OTEzMzVG; JSESSIONID=93D1D6346852288EB9636932796163A6; ',
            'Connection: keep-alive'
        ];

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate, br');

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            return [];
        }
        curl_close($ch);

        // Execute data.
        $data = json_decode($result, true);

        if ($page === 1) {
            $totalGoods = intval($data['result']['shop']['total_goods']);
            $returnData['total_page'] = ceil($totalGoods / 32);
        }
        $goodsList = [];

        foreach ($data['result']['goods_list'] as $goods) {
            $title = trim($goods['title']);
            if ($title === '') {
                continue;
            }

            // Get images.
            $images = [];
            if (!empty($goods['imgsSrc'])) {
                foreach ($goods['imgsSrc'] as $imgSrc) {
                    $imgTemp = explode('?', $imgSrc);
                    $images[] = trim($imgTemp[0]);
                }
            }

            $temp = explode('，', $title);
            $priceBlock = $temp[0];
            $price = '';

//            if (preg_match('/[0-9]+/', $priceBlock) && strpos($priceBlock, '-') !== false) {
//                $temp1 = explode('-', $priceBlock);
//                $priceBlock = $temp1[0];
//
//                for ($a = 0; $a < strlen($priceBlock); $a++) {
//                    if ($a % 2 === 1 && isset($priceBlock[$a])) {
//                        $price .= $priceBlock[$a];
//                    }
//                }
//            }

            $goodsList[] = [
                'szwego_shop_id' => $shopId,
                'page' => $page,
                'url' => trim($goods['link']),
                'title' => $title,
                'price' => intval($price),
                'images' => implode('|', $images),
                'post_time' => trim($goods['time']),
            ];
        }

        SzwegoProduct::insert($goodsList);

        return $returnData;
    }

    /**
     * @param $url
     * @param $prefix
     */
    private function getYupooProductList($url, $prefix)
    {
        $html = HtmlParser::getHTML($url);
        $products = [];

        // get pagination
        $pageNumbers = [];
        $pages = $html->find('div[class="none_select pagination__buttons"] a');
        foreach ($pages as $page) {
            $pageNumber = intval($page->plaintext);
            if ($pageNumber > 0) {
                $pageNumbers[] = $pageNumber;
            }
        }
        $maxPage = max($pageNumbers);

        for ($p = 1; $p <= $maxPage; $p++) {
            $result = $this->getYupooProductListByPage($url, $p);

            $maxPage = $result['maxPage'];
            $products = array_merge($products, $result['products']);
        }

        foreach ($products as $p) {
            $model = $this->getModelNumber($p['name']);
            print $prefix . $p['url'] . ',' . $p['name'] . ',' . $model . '<br>';
        }
    }

    /**
     * @param $productName
     * @return string
     */
    private function getModelNumber($productName)
    {
        $pattern1 = "/([a-zA-Z]{0,2})([0-9]{4,6})([\-\ ])([0-9]{1,3})/";
        $pattern2 = "/([a-zA-Z]{0,2})([0-9]{4,6})/";

        preg_match($pattern1, $productName, $matches);
        if (!isset($matches[0])) {
            preg_match($pattern2, $productName, $matches2);
            if (isset($matches2[0])) {
                return $matches2[0];
            } else {
                return '';
            }
        } else {
            return $matches[0];
        }
    }

    /**
     * Get product list from Yupoo.com by page.
     *
     * @param $url
     * @param $page
     * @return array
     */
    private function getYupooProductListByPage($url, $page)
    {
        $url .= '?page='.$page;
        $html = HtmlParser::getHTML($url);
        $productBoxes = $html->find('div[class="showindex__children"]');
        $products = [];

        // get pagination
        $pageNumbers = [];
        $pages = $html->find('div[class="none_select pagination__buttons"] a');
        foreach ($pages as $page) {
            $pageNumber = intval($page->plaintext);
            if ($pageNumber > 0) {
                $pageNumbers[] = $pageNumber;
            }
        }
        $maxPage = max($pageNumbers);

        foreach ($productBoxes as $box) {
            $productUrl = trim($box->find('a[class="album__main"]', 0)->href);
            $productName = str_replace(',', '', trim($box->find('div[class="text_overflow album__title"]', 0)->plaintext));
            $productName = str_replace('，', '', $productName);
            $products[] = [
                'url' => $productUrl,
                'name' => $productName,
            ];
        }

        return [
            'maxPage' => $maxPage,
            'products' => $products,
        ];
    }

    /**
     * Insert into crawl_pages.
     */
    public function insertCrawlPages()
    {
        /** Insert LISTING PRODUCT URLS and DOMAIN here... */
        $listPageUrls = [
            'http://www.usdcomewatch.com/BU-c46780/',
            'http://www.usdcomewatch.com/HBOSS-c46782/',
            'http://www.usdcomewatch.com/DZ-c46783/',
            'http://www.usdcomewatch.com/T-c46786/',
            'http://www.usdcomewatch.com/M-K-c46784/',
            'http://www.usdcomewatch.com/DW-c46785/'
        ];
        $domain = 'usdcomewatch.com';
        /** End of LISTING PRODUCT URLS and DOMAIN here... */

        $pages = [];
        $dbUrls = CrawlPage::pluck('url')->toArray();

        foreach ($listPageUrls as $url) {
            if (in_array($url, $dbUrls)) {
                continue;
            }

            $pages[] = [
                'domain' => $domain,
                'url' => $url,
            ];
        }
        CrawlPage::insert($pages);
    }

    /**
     * Crawling product from Shopify.
     */
    public function crawlShopify()
    {
        /*$context = stream_context_create(
            array(
                "http" => array(
                    "header" => "User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36"
                )
            )
        );
        $url = 'https://ground-tribes.myshopify.com/products.json?limit=1000&page=3';
        $browser = new phpWebHacks();
        $res = $browser->get($url);
        $rel = json_decode($res, true);
        dd($rel['products']);*/
    }

    /**
     * Get product links from usdcomewatch.com
     *
     * @param $url
     * @param CrawlPage $page
     * @param $getPage
     * @return array
     */
    public function usdcomewatchGetLink($url, CrawlPage $page, $getPage)
    {
        $url .= '?pagenum=' . $getPage;
        $html = HtmlParser::getHTML($url);
        $paginations = $html->find('ul[class="pagination"] li a');

        $pageNumbers = [];
        foreach ($paginations as $pagination) {
            $pageNumbers[] = intval(trim($pagination->plaintext));
        }
        $totalPage = max($pageNumbers);

        $links = $html->find('li[class="clearfix col-xs-6 col-md-3"]');
        $crawlLinks = [];
        $dbPageLinks = $page->crawlLinks()->pluck('url')->toArray();

        foreach ($links as $link) {
            $linkUrl = trim($link->find('span[class="products_title"] a', 0)->href);
            if (in_array($linkUrl, $dbPageLinks)) {
                continue;
            }
            $price = floatval(trim(str_replace('$', '', $link->find('span[class"products_price"] em', 0)->plaintext)));

            $crawlLinks[] = [
                'crawl_page_id' => $page->id,
                'url' => 'http://www.' . $page->domain . $linkUrl,
                'price' => $price,
            ];
        }

        CrawlLink::insert($crawlLinks);

        return [
            'totalPage' => $totalPage
        ];
    }

    /**
     * Get product detail from usdcomewatch.com
     *
     * @param CrawlLink $link
     * @return array
     */
    public function usdcomewatchGetProduct(CrawlLink $link)
    {
        $url = $link->url;
        $html = HtmlParser::getHTML($url);
        $nameTag = $html->find('h1[id="h_productname"]', 0);
        if (!$nameTag) {
            return [
                'productId' => 99999,
            ];
        }
        $name = trim($nameTag->plaintext);

        $image = '';
        $imageTag = $html->find('div[class="productshow_list"] a[class="swipebox_img swipebox"]', 0);
        if ($imageTag) {
            $image = trim($imageTag->href);
        }

        $description = trim($html->find('div[class="detailed_body"]', 0)->outertext());

        $product = CrawlFakeWatches::create(
            [
                'name' => $name,
                'images' => $image,
                'model_number' => '',
                'description' => $description,
                'crawl_link_id' => $link->id,
            ]
        );

        return [
            'productId' => $product->id,
        ];
    }

    /**
     * Get link for kickscrew.com.
     *
     * @param $url
     * @param CrawlPage $page
     * @param $getPage
     * @return array
     */
    public function kickscrewGetLink($url, CrawlPage $page, $getPage)
    {
        $url = $url . '?p=' . $getPage;
        $html = HtmlParser::getHTML($url);
        $ul = $html->find('ol[class*="product-items"]', 0);
        $links = [];
        $pageUrls = $page->crawlLinks()->pluck('url')->toArray();

        foreach ($ul->find('li[class*="product-item"]') as $k => $li) {
            $url = trim($li->find('a', 0)->href);

            // Check duplicate.
            if (in_array($url, $pageUrls)) {
                continue;
            }

            // Get price.
            $priceTag = $li->find('div[class*="price-box"] span[class="price-wrapper"]', 0);
            $price = !is_null($priceTag) ? floatval(trim(str_replace('$', '', str_replace(',', '', $priceTag->plaintext)))) : 0;

            $links[] = [
                'crawl_page_id' => $page->id,
                'url' => $url,
                'price' => $price,
            ];
        }

        // Insert crawl links.
        CrawlLink::insert($links);

        // Get total page.
        $paginationLi = $html->find('ul[class*="pages-items"] li[class*="item"]');
        $paginationNumbers = [];

        if (!empty($paginationLi)) {
            foreach ($paginationLi as $pagiLi) {
                $value = $pagiLi->find('span', 1)->plaintext;
                $paginationNumbers[] = intval(trim($value));
            }
            $totalPage = max($paginationNumbers);
        } else {
            $totalPage = $getPage;
        }

        return [
            'totalPage' => $totalPage
        ];
    }

    /**
     * @param CrawlLink $link
     * @return array
     */
    public function kickscrewGetProduct(CrawlLink $link)
    {
        $url = $link->url;
        $html = HtmlParser::getHTML($url);

        dd(count($html->find('img[class="fotorama__img--full"]')));

        // Get brand.
        $brandDiv = $html->find('div[class="product-brand"]', 0);
        $brand = !is_null($brandDiv) ? trim($brandDiv->plaintext) : '';

        // Get name.
        $nameTag = $html->find('h1[class="product-name"]', 0);
        $name = !is_null($nameTag) ? trim($nameTag->plaintext) : '';

        // Get description.
        $descriptionTag = $html->find('div[class="product-info"]', 0);
        $description = !is_null($descriptionTag) ? $descriptionTag->outertext() : '';

        // Get gender.
        $gender = '';
        $genderTag = $descriptionTag->find('table[id="product-attribute-specs-table"]', 0);
        foreach ($genderTag->find('tr') as $tr) {
            $head = trim($tr->find('th', 0)->plaintext);
            if ($head === 'Gender') {
                $gender = trim($tr->find('td', 0)->plaintext);
            }
        }

        // Get short description.
        $shortDescription = trim($genderTag->outertext());

        // Get images.
        $images = [];
        $imageTags = $html->find('div[class="product-gallery"] img[itemprop="image"]');
        foreach ($imageTags as $img) {
            $src = $img->getAttribute('src');
            if (trim($src) === '') {
                $src = $img->getAttribute('data-src');
            }
            $images[] = $src;
        }

        $product = \App\Models\CrawlProduct::create(
            [
                'crawl_link_id' => $link->id,
                'name' => $name,
                'short_description' => $shortDescription,
                'description' => $description,
                'price' => $link->price,
                'images' => implode('|', $images),
                'gender' => $gender,
                'brand' => $brand,
            ]
        );

        return [
            'productId' => $product->id,
        ];
    }

    /**
     * @return \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportCsv()
    {
        return (new CrawlFakeWatchesExport(2))->download('fake_watches.csv', Excel::CSV);
    }

    /**
     * @param $url
     * @param CrawlPage $page
     * @param $getPage
     * @return array
     */
    public function stadiumgoodsGetLink($url, CrawlPage $page, $getPage)
    {
        $url = $url . '/page/' . $getPage;
        $html = HtmlParser::getHTML($url);
        $ul = $html->find('ul[class="products-grid"]', 0);
        $links = [];
        $pageUrls = $page->crawlLinks()->pluck('url')->toArray();

        foreach ($ul->find('li') as $k => $li) {
            if ($k % 2 !== 0 || !is_object($li)) {
                continue;
            }

            $url = trim($li->find('a', 0)->href);

            // Check duplicate.
            if (in_array($url, $pageUrls)) {
                continue;
            }

            // Get price.
            $priceTag = $li->find('span[class="regular-price"]', 0);
            $price = !is_null($priceTag) ? floatval(trim(str_replace('$', '', str_replace(',', '', $priceTag->plaintext)))) : 0;

            $links[] = [
                'crawl_page_id' => $page->id,
                'url' => $url,
                'price' => $price,
            ];
        }

        // Insert crawl links.
        CrawlLink::insert($links);

        // Get total page.
        $paginationLi = $html->find('div[class="has-pagination"] li');
        $paginationNumbers = [];

        if (!empty($paginationLi)) {
            foreach ($paginationLi as $pagiLi) {
                $paginationNumbers[] = intval(trim($pagiLi->plaintext));
            }
            $totalPage = max($paginationNumbers);
        } else {
            $totalPage = $getPage;
        }

        return [
            'totalPage' => $totalPage
        ];
    }

    /**
     * @param CrawlLink $link
     * @return array
     */
    public function stadiumgoodsGetProduct(CrawlLink $link)
    {
        $url = $link->url;
        $html = HtmlParser::getHTML($url);

        // Get brand.
        $brandDiv = $html->find('div[class="product-brand"]', 0);
        $brand = !is_null($brandDiv) ? trim($brandDiv->plaintext) : '';

        // Get name.
        $nameTag = $html->find('h1[class="product-name"]', 0);
        $name = !is_null($nameTag) ? trim($nameTag->plaintext) : '';

        // Get description.
        $descriptionTag = $html->find('div[class="product-info"]', 0);
        $description = !is_null($descriptionTag) ? $descriptionTag->outertext() : '';

        // Get gender.
        $gender = '';
        $genderTag = $descriptionTag->find('table[id="product-attribute-specs-table"]', 0);
        foreach ($genderTag->find('tr') as $tr) {
            $head = trim($tr->find('th', 0)->plaintext);
            if ($head === 'Gender') {
                $gender = trim($tr->find('td', 0)->plaintext);
            }
        }

        // Get short description.
        $shortDescription = trim($genderTag->outertext());

        // Get images.
        $images = [];
        $imageTags = $html->find('div[class="product-gallery"] img[itemprop="image"]');
        foreach ($imageTags as $img) {
            $src = $img->getAttribute('src');
            if (trim($src) === '') {
                $src = $img->getAttribute('data-src');
            }
            $images[] = $src;
        }

        $product = \App\Models\CrawlProduct::create(
            [
                'crawl_link_id' => $link->id,
                'name' => $name,
                'short_description' => $shortDescription,
                'description' => $description,
                'price' => $link->price,
                'images' => implode('|', $images),
                'gender' => $gender,
                'brand' => $brand,
            ]
        );

        return [
            'productId' => $product->id,
        ];
    }
}


















