<?php

namespace App\Http\Controllers\Backend;

use App\Exports\CrawlProductExportToShopify;
use App\Exports\GmcFeedExport;
use App\Exports\ShopifyProductExport;
use App\Exports\WooProductExport;
use App\Http\Controllers\Controller;
use App\Models\CrawlProduct;
use App\Models\FanzixProduct;
use App\Models\WooProduct;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Excel;

class ConvertTemplateController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return $this->convertShopifyToWooProductCsv();
        return view('backend.convert');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function wooGmcFeed()
    {
        return view('backend.convert.woo_gmc_feed');
    }

    public function wooGmcFeedPost(Request $request)
    {
        if ($request->hasFile('excel_upload')) {
            $file = $request->file('excel_upload');
            $newFile = public_path('excel_convert/' . time() . rand(1000, 9999) . '.csv');
            move_uploaded_file($file->getRealPath(), $newFile);

            $data['excelContent'] = \fastexcel()->import($newFile)->toArray();
            \File::delete($newFile);
            $exportedFileName = !is_null($request->get('exported_name')) ? $request->get('exported_name') : 'gmc_feed_' . rand(100000, 999999);

            $data['replaceDomain'] = $request->get('replace_domain');
            $data['titlePrefix'] = $request->get('title_prefix');
            $data['titleSuffix'] = $request->get('title_suffix');
            $data['descriptionPrefix'] = $request->get('description_prefix');
            $data['descriptionSuffix'] = $request->get('description_suffix');
            $data['productType'] = $request->get('product_type');
            $data['applyImageUrl'] = $request->get('apply_image_url');
            $data['idPrefix'] = $request->get('prefix_id');

            return (new GmcFeedExport($data))->download($exportedFileName . '.csv', Excel::CSV);
        }

        return back();
    }

    public function convertShopifyToWooProductCsv()
    {
        $inputFileName = '[shopify_export]_tshirt.csv';
        $collection = \fastexcel()->import(public_path('excel_convert/' . $inputFileName));
        foreach ($collection as $item) {
            if (trim($item['Title']) === '') {
                continue;
            }

            $title = trim($item['Title']);
            $desc = trim($item['Body (HTML)']);
            $images = trim($item['Image Src']);
            $category = 'T-Shirt';
            $price = intval($item['Variant Price']);
            $wooSiteId = 8;

            $data[] = [
                'title' => $title,
                'woo_site_id' => $wooSiteId,
                'category' => $category,
                'price' => $price,
                'images' => $images,
                'description' => $desc,
            ];
        }

        WooProduct::insert($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function makeTemplateToShopify()
    {
        $collection = CrawlProduct::where('is_exported', 0)
            ->whereNotNull('manufacture_sku')
            ->where('price', '>=', 120)
            ->limit(800)
            ->get();

        return (new CrawlProductExportToShopify($collection))->download('fanyeez_' . rand(1000, 9999) . '.csv', Excel::CSV);
    }

    public function importFanzixProducts()
    {
        $collection = \fastexcel()->import(public_path('excel_convert/Clone-Fanzix-8.9.19.xlsx'));
        $insertedFanzixProducts = FanzixProduct::pluck('title')->toArray();

        $products = [];
        foreach ($collection as $item) {

            $title = $this->cleanTitle($item['Title']);
            if (in_array($title, $insertedFanzixProducts)) {
                continue;
            }

            $products[] = [
                'title' => $title,
                'regular_price' => $this->cleanMoney($item['Regular price']),
                'sale_price' => $this->cleanMoney($item['Sale price']),
                'description' => trim($item['Description']),
                'images' => trim($item['Images']),
            ];
        }

        FanzixProduct::insert($products);
        dd("Import " . count($products) . " products successfully.");
    }

    public function convertToolToShopifyTemplate()
    {
        $collection = FanzixProduct::where('image_checked', 1)
            ->where('is_exported', 0)
            ->where('import_error', 0)
            ->limit(2000)
            ->get();

        return (new ShopifyProductExport($collection))->download('FanzixProductExport_'. rand(1000, 9999) .'.csv', Excel::CSV);
    }

    public function convertWooProductExporter()
    {
        $collection = \fastexcel()->import(public_path('excel_convert/woocommerce-product-export.csv'))->toArray();
        return (new WooProductExport($collection))->download('fanzix_products.csv', Excel::CSV);
    }

    private function cleanTitle($title)
    {
        $title = trim($title);
        $title = str_replace('“', '', $title);
        $title = str_replace('”', '', $title);

        return remove_bs($title);
    }

    private function cleanMoney($money)
    {
        $money = trim($money);
        $money = str_replace('$', '', $money);

        return floatval($money);
    }
}
