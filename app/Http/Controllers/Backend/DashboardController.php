<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\Crawler\HtmlParser;
use App\Http\Controllers\Controller;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.dashboard');
    }

}
