<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FanzixProduct extends Model
{
    public $fillable = [
        'title',
        'regular_price',
        'sale_price',
        'description',
        'images',
        'image_checked',
        'is_exported',
        'import_error',
    ];
}
