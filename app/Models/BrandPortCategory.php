<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BrandPortCategory extends Model
{
    public $fillable = [
        'parent_id',
        'name',
        'url',
        'is_completed',
        'current_page',
        'total_page',
        'level',
    ];

    public function products()
    {
        return $this->hasMany('App\Models\BrandPortProduct');
    }
}
