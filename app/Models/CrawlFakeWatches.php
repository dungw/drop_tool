<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CrawlFakeWatches extends Model
{
    public $fillable = [
        'name',
        'crawl_link_id',
        'model_number',
        'description',
        'images'
    ];

    public function crawlLink()
    {
        return $this->belongsTo('App\Models\CrawlLink', 'crawl_link_id');
    }

    public static function getBrandRegexPattern($brand)
    {
        if ($brand === 'Emporio Armani') {
            return ['/((AR|Ar|ar|aR)[0-9]+)/'];
        } elseif ($brand === 'Burberry') {
            return ['/((BU|Bu|bU|bu)[0-9]+)/', '/(BBY[0-9]+)/'];
        } elseif ($brand === 'Hugo Boss') {
            return ['/([0-9]{4,})/'];
        } elseif ($brand === 'Diesel') {
            return ['/((DZ|dz|Dz|dZ)[0-9]+)/'];
        } elseif ($brand === 'Tissot') {
            return ['/((T|t)[0-9.]+)/'];
        } elseif ($brand === 'Michael Kors') {
            return ['/((MK|mk|Mk|mK)[0-9]+)/'];
        } elseif ($brand === 'Daniel Wellington') {
            return ['/((DW|dw|dW|Dw)[0-9]+)/'];
        }
    }
}
