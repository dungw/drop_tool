<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SzwegoShop extends Model
{
    public $fillable = [
        'url',
        'total_page',
        'cur_page',
        'status'
    ];

    public function szwegoProducts()
    {
        return $this->hasMany('App\Models\SzwegoProduct');
    }
}
