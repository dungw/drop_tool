<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BrandPortProduct extends Model
{
    public $fillable = [
        'category_id',
        'title',
        'short_desc',
        'description',
        'images',
        'price',
        'is_completed',
        'url',
    ];

    public function category()
    {
        return $this->belongsTo('App\Models\BrandPortCategory');
    }
}
