<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WooSite extends Model
{
    public $fillable = [
        'url',
        'no_shop_page',
        'only_first_image',
        'cur_page',
        'total_page',
        'status'
    ];

    public function products()
    {
        return $this->hasMany('App\Models\WooProduct');
    }
}
