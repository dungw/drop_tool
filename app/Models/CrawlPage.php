<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CrawlPage extends Model
{
    public $fillable = ['domain', 'url', 'total_page', 'current_page', 'is_completed'];

    public function crawlLinks()
    {
        return $this->hasMany('App\Models\CrawlLink');
    }
}
