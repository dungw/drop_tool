<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WooProduct extends Model
{
    public $fillable = [
        'woo_site_id',
        'url',
        'title',
        'short_desc',
        'description',
        'images',
        'price',
        'page_number',
        'is_checked',
        'is_marked',
        'tags',
        'category',
        'color',
        'size',
    ];

    public function wooSite()
    {
        return $this->belongsTo('App\Models\WooSite');
    }
}
