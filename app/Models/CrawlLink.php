<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CrawlLink extends Model
{
    public $fillable = ['crawl_page_id', 'is_completed', 'crawl_product_id', 'url', 'price'];

    public function crawlPage()
    {
        return $this->belongsTo('App\Models\CrawlPage');
    }

    public function crawlProduct()
    {
        return $this->hasOne('App\Models\CrawlProduct');
    }
}
