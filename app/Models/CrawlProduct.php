<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CrawlProduct extends Model
{
    const GENDER_MENS = 'Mens';
    const GENDER_WOMENS = 'Womens';
    const MENS_SHOES_SIZE_LIST = '6|6.5|7|7.5|8|8.5|9|9.5|10|10.5|11|11.5|12';
    const WOMENS_SHOES_SIZE_LIST = '5|5.5|6|6.5|7|7.5|8|8.5|9|9.5|10|10.5|11|11.5|12';
    const SKU_SHOES_CODE = 'S';

    public $fillable = ['crawl_link_id', 'name', 'short_description', 'description', 'price', 'images', 'gender', 'brand', 'is_exported', 'manufacture_sku'];

    public function crawlLink()
    {
        return $this->hasOne('App\Models\CrawlLink');
    }
}
