<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SzwegoProduct extends Model
{
    public $fillable = [
        'szwego_shop_id',
        'page',
        'url',
        'title',
        'price',
        'clean_title',
        'post_time',
        'status',
    ];

    public function szwegoShop()
    {
        $this->belongsTo('App\Models\SzwegoShop');
    }
}
