<?php

namespace App\Console;

use App\Console\Commands\CheckDuplicatedWooProduct;
use App\Console\Commands\CheckFanzixImage;
use App\Console\Commands\CleanSzwegoProductData;
use App\Console\Commands\CrawlLink;
use App\Console\Commands\CrawlProduct;
use App\Console\Commands\CrawlSzwego;
use App\Console\Commands\CrawlWooProduct;
use App\Console\Commands\CrawlWooProduct01;
use App\Console\Commands\CrawlWooSite;
use App\Console\Commands\CutOffWooProductTags;
use App\Console\Commands\FinishWooProductData;
use App\Console\Commands\GetBrandPortProduct;
use App\Console\Commands\GetBrandPortProductDetail;
use App\Console\Commands\GetMlbJerseyCategory;
use App\Console\Commands\RemoveImageLimitSizeRioJersey;
use App\Console\Commands\UpdateFakeWatchesModelNumber;
use App\Console\Commands\UpdateFanaticCentralImage;
use App\Console\Commands\UpdateManufactureSku;
use App\Console\Commands\UpdateWooProductShortDesc;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

/**
 * Class Kernel.
 */
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        CrawlLink::class,
        CrawlProduct::class,
        UpdateManufactureSku::class,
        UpdateFakeWatchesModelNumber::class,
        CheckFanzixImage::class,
        CrawlSzwego::class,
        CrawlWooSite::class,
        CrawlWooProduct::class,
        CrawlWooProduct01::class,
        CheckDuplicatedWooProduct::class,
        UpdateWooProductShortDesc::class,
        FinishWooProductData::class,
        CutOffWooProductTags::class,
        CleanSzwegoProductData::class,
        RemoveImageLimitSizeRioJersey::class,
        GetBrandPortProduct::class,
        GetBrandPortProductDetail::class,
        UpdateFanaticCentralImage::class,
        GetMlbJerseyCategory::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     */
    protected function schedule(Schedule $schedule)
    {
//        $schedule->command('crawl:link')->everyMinute();
//        $schedule->command('crawl:product')->everyMinute();
//        $schedule->command('update:manufacture_sku')->everyMinute();
//        $schedule->command('update:fake_watches_model_number')->everyMinute();
//        $schedule->command('check:fanzix_image')->everyMinute();
        // $schedule->command('crawl:woo_site')->everyMinute();
        $schedule->command('crawl:woo_product')->everyMinute();
        // $schedule->command('crawl:woo_product01')->everyMinute();
//        $schedule->command('check:dup_woo_pro')->everyMinute();
//        $schedule->command('update:woo_pro_short_desc')->everyMinute();
//        $schedule->command('finish:woo_product')->everyMinute();
//        $schedule->command('check:dup_woo_pro')->everyMinute();
//        $schedule->command('clean:szwego_product')->everyMinute();
//        $schedule->command('get:brandport_product')->everyMinute();
//        $schedule->command('get:brand_port_product_detail')->everyMinute();
//        $schedule->command('update:fnaticcentral_images')->everyMinute();
        // $schedule->command('get:mlb_category')->everyMinute();
//        $schedule->command('filter:mlb_category')->everyMinute();
    }

    /**
     * Register the commands for the application.
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
