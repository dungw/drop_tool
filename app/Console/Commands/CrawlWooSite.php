<?php

namespace App\Console\Commands;

use App\Models\WooSite;
use Illuminate\Console\Command;

class CrawlWooSite extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:woo_site';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function handle()
    {
        for ($i = 1; $i <= 5; $i++) {
            $wooSite = WooSite::where('status', 0)->first();
            $curPage = $wooSite->cur_page;
            $totalPage = $wooSite->total_page;
            if ($curPage >= $totalPage) {
                $wooSite->update(['status' => 1]);
            } else {
                $getPage = $curPage + 1;
                $controller = app()->make('App\Http\Controllers\Backend\CrawlController');

                // MLB Jersey.
                $function = 'getWooSitePage';
                if (strpos($wooSite->url, 'mlbjersey.online') !== false) {
                    $function = 'getMlbJerseyPage';
                } elseif (strpos($wooSite->url, 'fanatics.com') !== false) {
                    $function = 'getNflShopPage';
                } elseif (strpos($wooSite->url, 'www.nbaonlinejerseys.shop') !== false) {
                    $function = 'getNbaOnlineJerseysShop';
                } elseif (strpos($wooSite->url, 'ncaajerseys.store') !== false) {
                    $function = 'getNcaaStore';
                }

                $returnData = app()->call([$controller, $function], [
                    'wooSite' => $wooSite,
                    'getPage' => $getPage,
                ]);

                if (isset($returnData['total_page'])) {
                    $wooSite->update(
                        [
                            'total_page' => $returnData['total_page'],
                            'cur_page' => $getPage,
                        ]
                    );
                } else {
                    $wooSite->update(
                        [
                            'cur_page' => $getPage,
                        ]
                    );
                }
            }

//            sleep(5);
        }
    }
}
