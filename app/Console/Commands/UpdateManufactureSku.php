<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Sunra\PhpSimple\HtmlDomParser;

class UpdateManufactureSku extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:manufacture_sku';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update manufacture sku for Stadium Goods';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = \App\Models\CrawlProduct::where('manufacture_sku', 'none')->limit(20)->get();
        if ($products->isEmpty()) {
            die('All products are updated manufacture sku.');
        }

        foreach ($products as $product) {
            $manufactureSku = '';
            $shortDesc = $product->short_description;
            $html = HtmlDomParser::str_get_html($shortDesc);
            $trs = $html->find('table[id="product-attribute-specs-table"] tr');

            if (!empty($trs)) {
                foreach ($trs as $tr) {
                    $th = trim($tr->find('th', 0)->plaintext);
                    if ($th === 'Manufacturer Sku') {
                        $manufactureSku = trim($tr->find('td[class="data]', 0)->plaintext);
                    }
                }
            }

            if ($manufactureSku !== '') {
                $product->update(['manufacture_sku' => $manufactureSku]);
            }
        }
    }
}



























