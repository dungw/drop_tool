<?php

namespace App\Console\Commands;

use App\Helpers\Crawler\HtmlParser;
use App\Models\WooProduct;
use Illuminate\Console\Command;
use Sunra\PhpSimple\HtmlDomParser;

class FinishWooProductData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'finish:woo_product';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $allProducts = WooProduct::where('is_checked', 0)
//            ->where('id', 1029)
            ->limit(1000)
            ->get();

        foreach ($allProducts as $product) {
            // Update description.
            $removeStrings = [
                "Reviews  There are no reviews yet.  Be the first to review",
                "Your rating 12345  Your review*   Name*   Email*   Save my name, email, and website in this browser for the next time I comment.",
            ];
            $updateDesc = $description = $product->description;
            foreach ($removeStrings as $removeString) {
                if (strpos($description, $removeString) !== false) {
                    $updateDesc = trim(str_replace($removeString, '', $description));
                }
            }

            // Update tag.
            $tagArray = [];
            $title = strtolower(str_replace('&#8211;', '', $product->title));
            $a = explode(' ', $title);
            foreach ($a as $b) {
                $ti = ucwords(trim($b));
                if ($ti === '') {
                    continue;
                }
                $tagArray[] = $this->clean($ti);
            }

            if (!is_null($product->tags) || trim($product->tags) === '') {
                $t = explode('|', $product->tags);
                foreach ($t as $u) {
                    $tti = ucwords(strtolower(trim($u)));
                    if ($tti === '') {
                        continue;
                    }
                    $tagArray[] = $this->clean($tti);
                }
            }

            $tagArray = array_unique($tagArray);
            if (count($tagArray) > 10) {
                for ($i = 10; $i < count($tagArray); $i++) {
                    unset($tagArray[$i]);
                }
            }

            // Update short desc.
            $updateShortDesc = $product->short_desc;
            if (is_null($updateShortDesc) || trim($updateShortDesc) === '') {
                $pArray = [];
                $html = HtmlDomParser::str_get_html($product->description);
                $p = $html->find('p[data-tstid]');

                // Case 1.
                if (!is_null($p) && !empty($p)) {
                    foreach ($p as $p) {
                        $pArray[] = $p->outertext();
                    }
                    $updateShortDesc = implode('', $pArray);
                }
                // Case 2.
                else {
                    $pShortDesc = $html->find('div[class="product-short-description"]', 0);
                    if (!is_null($pShortDesc)) {
                        $updateShortDesc = trim($pShortDesc->innertext());
                    }
                }
            }

            $product->update(
                [
                    'tags' => implode('|', $tagArray),
                    'description' => $updateDesc,
                    'short_desc' => $updateShortDesc,
                    'is_checked' => 1,
                ]
            );
        }
    }

    private function clean($string)
    {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }
}
