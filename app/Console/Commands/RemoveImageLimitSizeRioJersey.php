<?php

namespace App\Console\Commands;

use App\Models\WooProduct;
use Illuminate\Console\Command;

class RemoveImageLimitSizeRioJersey extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:woo_product_category';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = WooProduct::where('woo_site_id', 4)
            ->where('title', 'LIKE', '%sneaker%')
            ->get();

        foreach ($products as $product) {
            $product->update(
                [
                    'category' => 'shoes',
                ]
            );
        }
    }
}




