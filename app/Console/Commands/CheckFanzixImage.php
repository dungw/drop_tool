<?php

namespace App\Console\Commands;

use App\Models\FanzixProduct;
use Illuminate\Console\Command;

class CheckFanzixImage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:fanzix_image';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = FanzixProduct::where('image_checked', 0)->limit(5)->get();
        foreach ($products as $product) {
            $checkedImages = [];
            $images = explode(',', $product->images);

            foreach ($images as $image) {
                $image = trim($image);

                if (image_exists($image)) {
                    $checkedImages[] = $image;
                }
            }

            if (empty($checkedImages)) {
                $checkedImages = ['https://fanzix.com/wp-content/uploads/2019/05/logo_icon-512x512.png'];
            }

            $product->update(
                [
                    'images' => implode(' , ', $checkedImages),
                    'image_checked' => 1,
                ]
            );
        }
    }
}
