<?php

namespace App\Console\Commands;

use App\Helpers\Crawler\HtmlParser;
use App\Models\WooProduct;
use Illuminate\Console\Command;

class GetMlbJerseyCategory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:mlb_category';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = WooProduct::where('woo_site_id', 9)
            ->where('category', 'MLB Jersey')
            ->where('is_marked', 0)
            ->limit(8)
            ->get();

        foreach ($products as $product) {
            dump($product->url);
            $html = HtmlParser::getHTML($product->url);
            $restrictWords = [
                'officially',
                'licensed',
                'made in',
                'brand',
                'replica',
                'nike',
                'authentic',
                'material',
                'legacy'
            ];

            $categories = [];
            $categoryLis = $html->find('div[class="mlb-shop-breadcrumb"] ul li');
            if (!is_null($categoryLis)) {
                foreach ($categoryLis as $li) {
                    $liContent = trim($li->plaintext);
                    $liContentLower = strtolower($liContent);
                    $willGet = true;
                    foreach ($restrictWords as $restrictWord) {
                        if (strpos($liContentLower, $restrictWord) !== false) {
                            $willGet = false;
                            break;
                        }
                    }

                    if ($willGet === true) {
                        $categories[] = $liContent;
                    }
                }
            }

            // If this is jersey.
            $categoryString = 'MLB-Unknown';
            if (count($categories) > 1) {
                $categoryString = trim($categories[count($categories) - 1]);
            }

            $product->update(
                [
                    'is_marked' => 1,
                    'category' => $categoryString,
                ]
            );

            sleep(4);
        }
    }
}
