<?php

namespace App\Console\Commands;

use App\Models\WooProduct;
use Illuminate\Console\Command;

class CutOffWooProductTags extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cut:woo_product_tags';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (WooProduct::all() as $wooProduct) {
            $finalTags = [];
            $tags = explode('|', $wooProduct->tags);
            $getTags = array_slice($tags, 0, 10);
            foreach ($getTags as $getTag) {
                $finalTags[] = trim(ucwords(strtolower($getTag)));
            }

            if (!empty($finalTags)) {
                $wooProduct->update(['tags' => implode('|', $finalTags)]);
            }
        }
    }
}
