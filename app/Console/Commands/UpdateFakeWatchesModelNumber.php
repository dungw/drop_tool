<?php

namespace App\Console\Commands;

use App\Models\CrawlFakeWatches;
use Illuminate\Console\Command;
use Sunra\PhpSimple\HtmlDomParser;

class UpdateFakeWatchesModelNumber extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:fake_watches_model_number';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update manufacture sku for Stadium Goods';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = \App\Models\CrawlFakeWatches::where('model_number', '')->limit(100)->get();
        if ($products->isEmpty()) {
            die('All products are updated model number.');
        }

        foreach ($products as $product) {
            $brand = $product->crawlLink->crawlPage->brand;
            $patterns = CrawlFakeWatches::getBrandRegexPattern($brand);

            $modelNumber = 'none';
            foreach ($patterns as $pattern) {
                $matches = [];
                preg_match($pattern, $product->name, $matches);
                if (isset($matches[1])) {
                    $modelNumber = $matches[1];
                    break;
                }
            }

            $product->update(['model_number' => $modelNumber]);
        }
    }
}



























