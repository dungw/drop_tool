<?php

namespace App\Console\Commands;

use App\Helpers\Crawler\HtmlParser;
use App\Models\BrandPortProduct;
use Illuminate\Console\Command;

class GetBrandPortProductDetail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:brand_port_product_detail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = BrandPortProduct::where('is_completed', 0)
            ->limit(20)
            ->get();

        foreach ($products as $product) {
            $url = $product->url;
            if (strpos($url, '&amp;') !== false) {
                $url = str_replace('&amp;', '&', $url);
            }
            dump($url);
            $html = HtmlParser::getHTML($url);

            // Get description.
            $descriptionBlock = $html->find('div[id="tab-description"]', 0);
            $description = trim($descriptionBlock->innertext());
            $description = str_replace('brandporterms.ru', '', $description);
            $description = str_replace('h1>', 'h3>', $description);
            $description = str_replace('/image/', 'https://www.brandporterms.ru/image/', $description);

            // Get images.
            $imageList = [];
            foreach ($descriptionBlock->find('img') as $imageTag) {
                $imageList[] = 'https://www.brandporterms.ru' . trim($imageTag->src);
            }

            $product->update(
                [
                    'images' => implode('|', $imageList),
                    'description' => $description,
                    'is_completed' => 1,
                ]
            );
        }
    }
}
