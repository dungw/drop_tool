<?php

namespace App\Console\Commands;

use App\Models\WooProduct;
use Illuminate\Console\Command;

class CrawlWooProduct01 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:woo_product01';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function handle()
    {
        $wooProducts = WooProduct::whereNull('description')
            ->limit(20)
            ->get();

        foreach ($wooProducts as $product) {
            $controller = app()->make('App\Http\Controllers\Backend\CrawlController');

            // MLB jersey.
            $function = 'getWooProduct';
            if (strpos($product->url, 'www.mlbjersey.online') !== false) {
                $function = 'getMlbJerseyDetail';
            } elseif (strpos($product->url, 'currentjoys') !== false) {
                $function = 'getCurrentJoyDetail';
            } elseif (strpos($product->url, 'nflshop.com') !== false) {
                $function = 'getNflShopDetail';
            } elseif (strpos($product->url, 'nbaonlinejerseys.shop') !== false) {
                $function = 'getNbaOnlineJerseysShopDetail';
            } elseif (strpos($product->url, 'ncaajerseys.store') !== false) {
                $function = 'getNcaaStoreDetail';
            }

            $returnData = app()->call([$controller, $function], [
                'wooProduct' => $product,
            ]);

            // sleep(4);
        }
    }
}
