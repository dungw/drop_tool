<?php

namespace App\Console\Commands;

use App\Http\Controllers\Backend\CrawlController;
use App\Models\SzwegoProduct;
use Illuminate\Console\Command;

class CleanSzwegoProductData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clean:szwego_product';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = SzwegoProduct::where('szwego_shop_id', 2)
            ->whereNull('clean_title')
            ->limit(45)
            ->get();

        foreach ($products as $product) {

            $price = 0;
            $cleanTitle = CrawlController::cleanChineseTitle($product->title);

            preg_match('/💰\s*([0-9]+)/', $cleanTitle, $matches);
            if (!empty($matches)) {
                $cleanTitle = str_replace($matches[0], '', $cleanTitle);
                $price = intval($matches[1]);
            }

            $product->update(
                [
                    'price' => $price,
                    'clean_title' => $cleanTitle,
                ]
            );
        }
    }
}
