<?php

namespace App\Console\Commands;

use App\Helpers\Crawler\HtmlParser;
use App\Models\BrandPortCategory;
use App\Models\BrandPortProduct;
use Illuminate\Console\Command;

class GetBrandPortProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:brandport_product';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $category = BrandPortCategory::where('is_completed', 0)
            ->where('level', 3)
            ->whereIn('parent_id', [73,82,89,92,104])
            ->first();

        $getPage = intval($category->current_page) + 1;
        $totalPage = intval($category->total_page);
        if ($getPage > $totalPage) {
            $category->update(['is_completed' => 1]);
            die;
        }

        $url = $category->url . '&page=' . $getPage;
        dump($url);
        $html = HtmlParser::getHTML($url);

        // Get products.
        $productArray = [];
        $productHolder = $html->find('div[class="grid_holder"] div[class="item contrast_font product-layout"]');
        foreach ($productHolder as $productBlock) {
            $aTag = $productBlock->find('div[class="information_wrapper"] div[class="name"] a', 0);
            $name = trim($aTag->plaintext);
            $url = trim($aTag->href);
            $price = doubleval(str_replace('$', '', trim($productBlock->find('div[class="information_wrapper"] div[class="price"]', 0)->plaintext)));
            $productArray[] = [
                'title' => $name,
                'url' => $url,
                'price' => $price,
                'category_id' => $category->id,
            ];
        }

        BrandPortProduct::insert($productArray);

        // Update total page.
        $updateTotalPage = 1;
        $pagination = $html->find('ul[class="pagination"]', 0);
        if (!is_null($pagination)) {
            $pageArr = [];
            foreach ($pagination->find('li') as $liPage) {
                $pageArr[] = intval(trim($liPage->plaintext));
            }
            $updateTotalPage = max($pageArr);
        }
        $category->update(
            [
                'total_page' => $updateTotalPage,
                'current_page' => $getPage,
            ]
        );

    }
}



