<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\CrawlLink;

class CrawlProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:product';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crawl product detail';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $links = CrawlLink::where('is_completed', 0)->limit(10)->get();
        if ($links->isEmpty()) {
            $this->info('All links are completed.');
            die;
        }

        foreach ($links as $k => $link) {
            $this->info($link->url);
            $this->execTask($link);
            $this->info('done');
            sleep(2);
        }
        $this->info('End of calling crawl product...');
    }

    private function execTask(CrawlLink $link)
    {
        $controller = app()->make('App\Http\Controllers\Backend\CrawlController');
        $domain = $link->crawlPage->domain;
        $a = explode('.', trim($domain));
        $functionName = $a[0] . 'GetProduct';

        $returnData = app()->call([$controller, $functionName], [
            'link' => $link,
        ]);

        $link->update(
            [
                'is_completed' => 1,
                'crawl_product_id' => $returnData['productId'],
            ]
        );
    }
}
































