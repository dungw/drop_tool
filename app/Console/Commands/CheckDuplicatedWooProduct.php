<?php

namespace App\Console\Commands;

use App\Models\WooProduct;
use Illuminate\Console\Command;

class CheckDuplicatedWooProduct extends Command
{
    const LIMIT = 1000;
    const SHOP_ID = 1;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:dup_woo_pro';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        for ($i = 1; $i <= 20; $i++) {
            $checkProducts = WooProduct::select(['id', 'title', 'price', 'description', 'is_checked'])
//                ->where('woo_site_id', self::SHOP_ID)
                ->where('is_checked', 0)
                ->where('is_marked', 0)
                ->where('woo_site_id', '<>', 4)
                ->limit(1)
                ->get();

            $allProducts = WooProduct::select(['id', 'title', 'price', 'description', 'is_marked'])
//                ->where('woo_site_id', self::SHOP_ID)
                ->where('is_checked', 0)
                ->where('is_marked', 0)
                ->where('woo_site_id', '<>', 4)
                ->get();

            foreach ($checkProducts as $k => $wooProduct) {
//                $subDesc = substr(trim($wooProduct->description), 0, 600);

                foreach ($allProducts as $wooP) {
                    if ($wooP->id === $wooProduct->id) {
                        continue;
                    }

//                    $wooPDesc = trim($wooP->description);
                    if (trim($wooProduct->title) === trim($wooP->title)) {
                        $wooP->update(
                            [
                                'is_marked' => 1
                            ]
                        );
                    }
                }

                $wooProduct->update(
                    [
                        'is_checked' => 1
                    ]
                );
            }
        }
    }
}
