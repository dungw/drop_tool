<?php

namespace App\Console\Commands;

use App\Helpers\Crawler\HtmlParser;
use App\Models\WooProduct;
use Illuminate\Console\Command;

class UpdateWooProductShortDesc extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:woo_pro_short_desc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = WooProduct::where('woo_site_id', 3)
            ->whereNull('tags')
            ->limit(15)
            ->get();

        foreach ($products as $product) {
            $url = $product->url;
            dump($url);
            $html = HtmlParser::getHTML($url);

            // Get short desc.
            $updateShortDesc = $product->short_desc;
            $shortDescDiv = $html->find('div[class="product-short-description"]', 0);
            if (!is_null($shortDescDiv)) {
                $updateShortDesc = $shortDescDiv->outertext();
            }

            // Get tags.
            $updateTag = '';
            $tagBlock = $html->find('span[class="tagged_as"]', 0);
            if (!is_null($tagBlock)) {
                foreach ($tagBlock->find('a') as $tagA) {
                    $tagArr[] = trim($tagA->plaintext);
                }
                $updateTag = implode('|', $tagArr);
            }

            $product->update(
                [
                    'short_desc' => $updateShortDesc,
                    'tags' => $updateTag,
                ]
            );
        }
    }
}
