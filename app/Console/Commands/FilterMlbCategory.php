<?php

namespace App\Console\Commands;

use App\Models\WooProduct;
use Illuminate\Console\Command;

class FilterMlbCategory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'filter:mlb_category';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = WooProduct::where('woo_site_id', 9)
            ->where('category', '!=', 'MLB Jersey')
            ->where('is_marked', 0)
            ->get();

        foreach ($products as $product) {
            $temp = explode('>', $product->category);
            $lastPart = $temp[count($temp)-1];
            $product->update(
                [
                    'is_marked' => 1,
                    'category' => $lastPart,
                ]
            );
        }
    }
}
