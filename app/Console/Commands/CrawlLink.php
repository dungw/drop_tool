<?php

namespace App\Console\Commands;

use App\Models\CrawlPage;
use Illuminate\Console\Command;

class CrawlLink extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:link';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get links from page';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Calling crawl link...');
        $page = CrawlPage::where('is_completed', 0)->first();
        if (is_null($page)) {
            $this->info('All pages are completed.');
            die;
        }

        if ($page->current_page === $page->total_page) {
            $page->update(
                ['is_completed' => 1]
            );
        } else {
            $getPage = $page->current_page + 1;

            $this->info('>> Getting page: ' . $page->url);
            $this->info('>> Get page : ' . $getPage);
            $this->info('>> Total page : ' . $page->total_page);

            $a = explode('.', trim($page->domain));
            $functionName = $a[0] . 'GetLink';
            $controller = app()->make('App\Http\Controllers\Backend\CrawlController');

            $returnData = app()->call([$controller, $functionName], [
                'url' => $page->url,
                'page' => $page,
                'getPage' => $getPage,
            ]);

            // Update current page & total page.
            $totalPage = $page->total_page;

            if ($totalPage < $returnData['totalPage']) {
                $totalPage = $returnData['totalPage'];
            }

            $page->update(
                [
                    'current_page' => $getPage,
                    'total_page' => $totalPage,
                ]
            );
        }

        $this->info('End of calling crawl link...');
    }
}





























