<?php

namespace App\Console\Commands;

use App\Models\SzwegoShop;
use Illuminate\Console\Command;

class CrawlSzwego extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:szwego';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        for ($i = 1; $i <= 20; $i++) {
            $crawlingShop = SzwegoShop::where('status', 0)->first();
            $curPage = intval($crawlingShop->cur_page);
            $totalPage = intval($crawlingShop->total_page);

            if ($curPage > $totalPage) {
                $crawlingShop->update(['status' => 1]);
            } else {
                $getPage = $curPage + 1;

                // Main execution.
                $controller = app()->make('App\Http\Controllers\Backend\CrawlController');
                $returnData = app()->call([$controller, 'getSzwegoByPage'], [
                    'shopId' => $crawlingShop->id,
                    'shopUrl' => $crawlingShop->url,
                    'page' => $getPage
                ]);

                if (isset($returnData['total_page'])) {
                    $crawlingShop->update(
                        [
                            'cur_page' => $getPage,
                            'total_page' => $returnData['total_page']
                        ]
                    );
                } else {
                    $crawlingShop->update(
                        [
                            'cur_page' => $getPage,
                        ]
                    );
                }
            }
        }
    }
}
