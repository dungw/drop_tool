<?php

namespace App\Console\Commands;

use App\Models\WooProduct;
use Illuminate\Console\Command;

class UpdateFanaticCentralImage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:fnaticcentral_images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = WooProduct::where('woo_site_id', 5)
            ->where('is_marked', 0)
            ->limit(100)
            ->get();

        if ($products->isEmpty()) {
            die('Done.');
        }
        foreach ($products as $product) {
            if (trim($product->images) === '') {
                $product->delete();
                continue;
            } else {
                $images = explode('|', trim($product->images));
                foreach ($images as $k => $image) {
                    $parts = explode('-', $image);
                    $lastPart = $parts[count($parts) - 1];
                    if (strpos($lastPart, '.') === false) {
                        continue;
                    }
                    $part1s = explode('.', $lastPart);
                    unset($parts[count($parts) - 1]);
                    $ext = $part1s[1];
                    $newImage = implode('-', $parts) . '.' . $ext;

                    $images[$k] = $newImage;
                }
            }

            $product->update(
                [
                    'images' => implode('|', $images),
                    'is_marked' => 1,
                ]
            );
        }
    }
}
