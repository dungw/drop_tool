@extends('backend.layouts.app')

@section('title', app_name() . ' | Convert Woo exported products to GMC feed')

@section('content')
    {{ html()->form('POST', url('admin/convert/woo_gmc_feed_post'))
        ->class('form-horizontal')
        ->acceptsFiles()
        ->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        Convert Woo Product Export to GMC Feed
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4">
                <div class="col">

                    <div class="form-group row">
                        {{ html()->label('Woo product file')->class('col-md-2 form-control-label')->for('excel_upload') }}

                        <div class="col-md-10">
                            {{ html()->input('file', 'excel_upload')->class('form-control')->required()->autofocus() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label('ID Prefix')->class('col-md-2 form-control-label')->for('prefix_id') }}

                        <div class="col-md-10">
                            {{ html()->input('text', 'prefix_id')->class('form-control') }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label('Domain')->class('col-md-2 form-control-label')->for('replace_domain') }}

                        <div class="col-md-10">
                            {{ html()->input('text', 'replace_domain')->class('form-control') }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label('Product Type')->class('col-md-2 form-control-label')->for('product_type') }}

                        <div class="col-md-10">
                            {{ html()->select('product_type', [
                                    'variation' => 'Variation',
                                    'simple' => 'Simple',
                                ])->class('form-control') }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label('Apply for image URLs')->class('col-md-2 form-control-label')->for('apply_image_url') }}

                        <div class="col-md-10">
                            {{ html()->checkbox('apply_image_url')->class('form-control') }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label('Export file name')->class('col-md-2 form-control-label')->for('exported_name') }}

                        <div class="col-md-10">
                            {{ html()->input('text', 'exported_name')->class('form-control')->value('gmc_feed_' . rand(100000, 999999)) }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label('Title prefix')->class('col-md-2 form-control-label')->for('title_prefix') }}

                        <div class="col-md-10">
                            {{ html()->input('text', 'title_prefix')->class('form-control') }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label('Title suffix')->class('col-md-2 form-control-label')->for('title_suffix') }}

                        <div class="col-md-10">
                            {{ html()->input('text', 'title_suffix')->class('form-control') }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label('Description prefix')->class('col-md-2 form-control-label')->for('description_prefix') }}

                        <div class="col-md-10">
                            {{ html()->input('text', 'description_prefix')->class('form-control') }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label('Description suffix')->class('col-md-2 form-control-label')->for('description_suffix') }}

                        <div class="col-md-10">
                            {{ html()->input('text', 'description_suffix')->class('form-control') }}
                        </div><!--col-->
                    </div><!--form-group-->

                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.auth.role.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit('Submit') }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection
