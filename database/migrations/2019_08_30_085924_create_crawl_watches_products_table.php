<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrawlWatchesProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crawl_fake_watches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('crawl_link_id')->unsigned();
            $table->string('name');
            $table->string('model_number')->nullable()->default(null);
            $table->text('description')->nullable()->default(null);
            $table->text('images')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crawl_fake_watches');
    }
}
