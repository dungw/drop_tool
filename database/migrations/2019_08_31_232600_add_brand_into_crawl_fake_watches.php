<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBrandIntoCrawlFakeWatches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crawl_fake_watches', function (Blueprint $table) {
            $table->string('brand')->nullable()->default(null)->after('name');
        });

        Schema::table('crawl_pages', function (Blueprint $table) {
            $table->string('brand')->nullable()->default(null)->after('url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crawl_fake_watches', function (Blueprint $table) {
            $table->dropColumn('brand');
        });

        Schema::table('crawl_pages', function (Blueprint $table) {
            $table->dropColumn('brand');
        });
    }
}
