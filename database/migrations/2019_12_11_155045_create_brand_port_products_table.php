<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandPortProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brand_port_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('category_id')->unsigned();
            $table->string('title')->nullable()->default(null);
            $table->text('short_desc')->nullable()->default(null);
            $table->mediumText('description')->nullable()->default(null);
            $table->text('images')->nullable()->default(null);
            $table->string('price')->nullable()->default(null);
            $table->tinyInteger('is_completed')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brand_port_products');
    }
}
