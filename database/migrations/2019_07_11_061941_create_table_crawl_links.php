<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCrawlLinks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crawl_links', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('crawl_page_id')->unsigned();
            $table->tinyInteger('is_completed')->nullable()->default(0);
            $table->integer('crawl_product_id')->unsigned()->nullable();
            $table->string('url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crawl_links');
    }
}
