<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandPortCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brand_port_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('parent_id')->nullable()->default(0);
            $table->string('name');
            $table->text('url')->nullable()->default(null);
            $table->integer('current_page')->nullable()->default(0);
            $table->integer('total_page')->nullable()->default(1);
            $table->tinyInteger('is_completed')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brand_port_categories');
    }
}
