<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrawlWooProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('woo_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('woo_site_id')->unsigned();
            $table->string('title')->nullable()->default(null);
            $table->string('url')->nullable()->default(null);
            $table->text('short_desc')->nullable()->default(null);
            $table->text('description')->nullable()->default(null);
            $table->float('price')->nullable()->default(null);
            $table->text('images')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('woo_products');
    }
}
