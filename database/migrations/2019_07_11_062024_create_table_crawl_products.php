<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCrawlProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crawl_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('crawl_link_id')->unsigned();
            $table->string('name');
            $table->text('short_description')->nullable()->default(null);
            $table->text('description')->nullable()->default(null);
            $table->float('price')->nullable()->default(0);
            $table->text('images')->nullable()->default(null);
            $table->string('gender')->nullable()->default(null);
            $table->string('brand')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crawl_products');
    }
}
