<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimeIntoSzwegoProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('szwego_products', function (Blueprint $table) {
            $table->string('post_time')->nullable()->default(null)->after('url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('szwego_products', function (Blueprint $table) {
            $table->dropColumn('post_time');
        });
    }
}
