<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSzwegoProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('szwego_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('szwego_shop_id')->unsigned();
            $table->integer('page');
            $table->string('url');
            $table->text('title');
            $table->string('price')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('szwego_products');
    }
}
