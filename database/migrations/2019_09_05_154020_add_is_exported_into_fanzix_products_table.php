<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsExportedIntoFanzixProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fanzix_products', function (Blueprint $table) {
            $table->tinyInteger('is_exported')->after('image_checked')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fanzix_products', function (Blueprint $table) {
            $table->dropColumn('is_exported');
        });
    }
}
