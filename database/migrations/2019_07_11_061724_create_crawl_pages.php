<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrawlPages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crawl_pages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('domain')->nullable()->default(null);
            $table->string('url');
            $table->integer('total_page')->nullable()->default(1);
            $table->integer('current_page')->nullable()->default(0);
            $table->tinyInteger('is_completed')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crawl_pages');
    }
}
