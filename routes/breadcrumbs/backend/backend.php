<?php

Breadcrumbs::for('admin.dashboard', function ($trail) {
    $trail->push(__('strings.backend.dashboard.title'), route('admin.dashboard'));
});

Breadcrumbs::for ('admin.convert.shopify_template', function ($trail) {
    $trail->push(__('Convert to Shopify template'), route('admin.dashboard'));
});

Breadcrumbs::for('admin.convert', function ($trail) {
    $trail->push(__('Convert Template Page'), route('admin.convert'));
});

Breadcrumbs::for('admin.convert.woo_gmc_feed', function ($trail) {
    $trail->push(__('Convert Woo Product Export to GMC Feed'), route('admin.convert.woo_gmc_feed'));
});

require __DIR__.'/auth.php';
require __DIR__.'/log-viewer.php';
