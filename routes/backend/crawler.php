<?php
use App\Http\Controllers\Backend\CrawlController;

Route::group([
    'prefix' => 'crawler',
    'as' => 'crawler.',
], function () {
    Route::get('/', [CrawlController::class, 'index']);
    Route::get('/export_csv', [CrawlController::class, 'exportCsv']);
    Route::get('/shopify', [CrawlController::class, 'crawlShopify']);
});
