<?php

use App\Http\Controllers\Backend\ConvertTemplateController;

Route::group([
    'prefix' => 'convert',
    'as' => 'convert.',
], function () {
    Route::get('/woo_gmc_feed', [ConvertTemplateController::class, 'wooGmcFeed'])->name('woo_gmc_feed');
    Route::post('/woo_gmc_feed_post', [ConvertTemplateController::class, 'wooGmcFeedPost'])->name('woo_gmc_feed_post');

    Route::get('/crawl_product_to_shopify', [ConvertTemplateController::class, 'makeTemplateToShopify'])->name('crawl_product_to_shopify');
    Route::get('/import_fanzix_product', [ConvertTemplateController::class, 'importFanzixProducts']);
    Route::get('/shopify_template', [ConvertTemplateController::class, 'convertToolToShopifyTemplate'])->name('shopify_template');
    Route::get('/woo_product_exporter', [ConvertTemplateController::class, 'convertWooProductExporter'])->name('woo_product_exporter');
    Route::get('/shopify_to_woo_product_csv', [ConvertTemplateController::class, 'convertShopifyToWooProductCsv'])->name('shopify_to_woo_product_csv');
});
